package main.java.com.login;

import java.util.HashMap;

import org.apache.log4j.Logger;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class Login extends GenericClass {

	public static Logger log = Logger.getLogger(Login.class.getName());

	public void login(String testName, String user) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();	
		try {			
			HashMap<String, String> Logindata = getData(Constants.INPUT_XLS, "Login", testName);
			SM_LoadLogger();
			driver = SM_BrowserLaunch(prop.getProperty("browser"), prop.getProperty("Application_URL"));
			if (user.equalsIgnoreCase("Processor_ID")) {
				input("User_ID_xpath", Logindata.get("Processor_ID"));
				input("Password_xpath", Logindata.get("Processor_Password"));
			} else if (user.equalsIgnoreCase("Approver_ID")) {
				input("User_ID_xpath", Logindata.get("Approver_ID"));
				input("Password_xpath", Logindata.get("Approver_Password"));
			}
			Thread.sleep(2000);
			click("Enter_xpath");
			explicitwait("World_xpath", 300, "visibilityOfElementLocated");
			String status = verifyElementPresent("World_xpath");
			if (status.equalsIgnoreCase("Pass")) {
				driver.switchTo().defaultContent();
				reportingPass(logger, "successfully logged In", testName, nameofCurrMethod);
			} else {
				driver.switchTo().defaultContent();
				reportingFail(logger, "Login Unsuccessfully", testName, nameofCurrMethod);
			}
			Thread.sleep(5000);
		} catch (Exception e) {
			//reportingFail(logger, "Login Unsuccessfully", testName, nameofCurrMethod);
		}
	}
}
