package main.java.com.GenericClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import main.java.com.login.Login;

public class GenericClass {
	public static Logger log = Logger.getLogger(GenericClass.class.getName());
	public static WebDriver driver = null;
	public static int counter = 1;
	public static ExtentTest logger = null;
	public static WebElement element;
	static String updatedReportPath; 
	public static Properties prop = null;
	Set<String> allwindows = null;
	static String timeStamp;
	static String updatedReportFile;
	static String updatedPassFolder;
	static String updatedFailFolder;
	protected static String functionStatus;
	
	
	public static ExtentReports reports = new ExtentReports(System.getProperty("user.dir") + "\\Reports\\v10TestCasesReports.html");

	public static void GenericClassMethod() {
		prop = new Properties();
		System.out.println("generic constructor--------------------------------------------");
		try {
			FileInputStream LoginProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Login.properties");
			prop.load(LoginProperties);

			FileInputStream configProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\main\\resource\\config.properties");
			prop.load(configProperties);

			FileInputStream HomePageProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\HomePageProperties.properties");
			prop.load(HomePageProperties);

			FileInputStream PersonProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Person.properties");
			prop.load(PersonProperties);

			FileInputStream CommonProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Common.properties");
			prop.load(CommonProperties);

			FileInputStream ContractsCertsProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\ContractsAndCerts.properties");
			prop.load(ContractsCertsProperties);

			FileInputStream AnnuitantProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Annuitant.properties");
			prop.load(AnnuitantProperties);

			FileInputStream BenefitsProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Benefits.properties");
			prop.load(BenefitsProperties);
			
			FileInputStream mainPageProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\mainPage.properties");
			prop.load(mainPageProperties);
			
			FileInputStream DocumentationProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Documentation.properties");
			prop.load(DocumentationProperties);

			FileInputStream PaymentHistoryProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\PaymentHistory.properties");
			prop.load(PaymentHistoryProperties);			
			
			FileInputStream TransactionProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Transaction.properties");
			prop.load(TransactionProperties); 
			
			FileInputStream PaymentSetupProperties = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\PaymentSetup.properties");
			prop.load(PaymentSetupProperties);
			
			FileInputStream SideArrowWindow = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\SideArrowWindow.properties");
			prop.load(SideArrowWindow);	
			
			FileInputStream FactoringPage = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\Factoring.properties");
			prop.load(FactoringPage);	
			
			FileInputStream FactoringModeling = new FileInputStream(
					System.getProperty("user.dir") + "\\src\\test\\resource\\FactoringModelingTools.properties");
			prop.load(FactoringModeling);	
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void reporting() {

		timeStamp = new SimpleDateFormat("yyyyMMMddhhmm").format(Calendar.getInstance().getTime());
		
		// create main folder of report
		File reportFile = new File(System.getProperty("user.dir") + "\\Reports\\" + timeStamp);
		reportFile.mkdir();
		updatedReportFile = System.getProperty("user.dir") + "\\Reports\\" + timeStamp;
		reports = new ExtentReports(updatedReportFile+"\\v10TestCasesReports.html");

		// create pass folder of report
		File passFolder = new File(updatedReportFile + "\\Passed_Screenshots");
		passFolder.mkdir();
		updatedPassFolder = updatedReportFile + "\\Passed_Screenshots";

		// create fail folder of report
		File failFolder = new File(updatedReportFile + "\\Failed_Screenshots");
		failFolder.mkdir();
		updatedFailFolder = updatedReportFile + "\\Failed_Screenshots";

	}
	
	public static void deletePreviousReportingFolder() {
		File previousReportFolder = new File(System.getProperty("user.dir") + "\\Reports\\"); 

		if (previousReportFolder.exists()) { 		

			File[] previousReportFolderList = previousReportFolder.listFiles();			
			File projectDirectory = new File(System.getProperty("user.dir"));
			String projectParentDirectory = projectDirectory.getParent();
		      //Creating the directory
			File localDirectoryForReport = new File(projectParentDirectory+"\\Automation date wise report");
		    localDirectoryForReport.mkdir();
			for (File file : previousReportFolderList) {
				System.out.println(file);
				if (file.renameTo(new File(localDirectoryForReport+"\\"+file.getName()))) {
					// if file copied successfully then delete the original file
					file.delete();
					System.out.println("File moved successfully");
				} else {
					System.out.println("Failed to move the file");
				}
			}
		}
	}

	public static void clearBrowserCache() {
		driver.manage().deleteAllCookies();
	}

	public static void getURL(String url) {
		driver.get(url);
	}

	public static WebDriver SM_BrowserLaunch(String BrowserName, String url) throws InterruptedException {
		if (BrowserName.equalsIgnoreCase("ff")) {
			driver = new FirefoxDriver();
			
		} else if (BrowserName.equalsIgnoreCase("ch")) {
			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (BrowserName.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "drivers\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		} else if (BrowserName.equalsIgnoreCase("op")) {
			System.setProperty("webdriver.opera.driver", "drivers\\operadriver.exe");
			driver = new OperaDriver();
		}
		driver.manage().window().maximize();
		clearBrowserCache();
		driver.navigate().to(url);
		driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		return driver;

	}

	public static void SM_closeBrowser() {
		driver.quit();
	}

	public static Properties SM_propertiesReader(String filepath) {
		Properties propobj = new Properties();
		try (FileInputStream fis = new FileInputStream(filepath)) {
			propobj.load(fis);
		} catch (IOException ie) {
			ie.printStackTrace();
		}
		return propobj;
	}

	public static void SM_LoadLogger() throws Exception {
		try (FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resource\\log4j.properties")) {
			Properties p = new Properties();
			p.load(fis);
			PropertyConfigurator.configure(p);
		}
	}

	public static void SM_chooseRadioButton(int n, boolean bValue, String xpath) {
		List<WebElement> oRadioButton = driver.findElements(By.xpath(xpath));
		bValue = oRadioButton.get(1).isSelected();
		if (bValue == true) {
			oRadioButton.get(1).click();
		} else {
			oRadioButton.get(2).click();
		}
	}

	public static int randomNumberInt(int n) {
		Random rand = new Random();
		int i = rand.nextInt(n);
		return i;
	}

	public static void SM_selectFromMenu(WebElement we, String Text) {
		if (driver.findElement(By.cssSelector("#nav > label")).isDisplayed()) {
			driver.findElement(By.cssSelector("#nav > label")).click();
		}
		we.click();
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver, 1000);
		WebElement dropdownitem = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(Text)));
		actions.moveToElement(dropdownitem);
		actions.click().build().perform();
	}

	public void SM_doubleClick(WebElement element) {
		Actions action = new Actions(driver);
		action.doubleClick(element).perform();
	}

	public static String takeScreenShotFail(WebDriver driver, String testName, String nameofCurrMethod) throws InterruptedException {
		String filepath = null;
		try {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			filepath =  updatedFailFolder+"\\"+testName+" "+nameofCurrMethod+ ".png";
			FileUtils.copyFile(src, new File(filepath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filepath;
	}

	public static void reportingFail(ExtentTest logger, String failMsg, String testName, String nameofCurrMethod) throws Exception {
		log.info("FAIL: " + failMsg);
		logger.log(LogStatus.FAIL, failMsg);
		String filename = takeScreenShotFail(driver, testName, nameofCurrMethod);
		String fname[] = filename.split("Failed_Screenshots");
		String imagePath = "Failed_Screenshots\\" + fname[1];
		logger.log(LogStatus.FAIL, "" + logger.addScreenCapture(imagePath));

	}

	public static String takeScreenShotPass(WebDriver driver, String testName, String nameofCurrMethod) throws InterruptedException {
		String filepath = null;
		try {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			filepath = updatedPassFolder+"\\"+testName+" "+nameofCurrMethod+ ".png";
			FileUtils.copyFile(src, new File(filepath));
			} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("File path is : " + filepath);
		return filepath;
	}

	public static void reportingPass(ExtentTest logger, String passMsg,  String testName, String nameofCurrMethod) throws Exception {

		// Reporter.log("Pass: "+passMsg);
		// log.info("Pass "+passMsg);
		logger.log(LogStatus.PASS, passMsg);
		String filename = takeScreenShotPass(driver, testName, nameofCurrMethod);
		/*String fname[] = filename.split("Passed_Screenshots");
		String imagePath = "Passed_Screenshots\\" + fname[1];
		logger.log(LogStatus.PASS, ""+ logger.addScreenCapture(imagePath));*/
	}

	public static void switchToFrame(String FrameName) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(FrameName);

	}

	public static void switchToTwoFrame(String Frame1, String Frame2) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(Frame1);
		driver.switchTo().frame(Frame2);
	}

	public Set<String> gethandles() {
		Set<String> alwindows = driver.getWindowHandles();
		return alwindows;
	}

	public void popuphandler(String title) {
		allwindows = gethandles();
		for (String childwindow : allwindows) {
			driver.switchTo().window(childwindow);
			String ActualWindowTitle = driver.getTitle();
			System.out.println("ActualWindowTitle : " + ActualWindowTitle);

			if (ActualWindowTitle.contains(title)) {
				break;
			}
		}
	}

	public void popuphandler() {
		allwindows = gethandles();
		for (String childwindow : allwindows) {
			driver.switchTo().window(childwindow);
		}
	}

	public void switchToDiv(String locator) {
		new Actions(driver).click(getElement(locator)).perform();

	}

	public void explicitwait(String locator, int timeoutinseconds, String condition) {

		if (condition.equalsIgnoreCase("visibilityOfElementLocated")) {
			new WebDriverWait(driver, timeoutinseconds)
					.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty(locator))));
		}
		if (condition.equalsIgnoreCase("ElementToBeClickable")) {
			new WebDriverWait(driver, timeoutinseconds)
					.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty(locator))));
		}
		if (condition.equalsIgnoreCase("FrameToBeAvailableAndSwitchToIt")) {
			new WebDriverWait(driver, timeoutinseconds)
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(prop.getProperty(locator))));
		}
	}

	public void mouseHover(String locatorKey) {
		WebElement e = getElement(locatorKey);
		Actions builder = new Actions(driver);
		builder.moveToElement(e).click().perform();
	}

	public WebElement getElementinalist(String locatorKey, String elementtoget) throws InterruptedException   {
		WebElement particularElement = null;
		if (locatorKey.endsWith("_xpath")) {
			List<WebElement> elements = driver.findElements(By.xpath(prop.getProperty(locatorKey)));
			for (int i = 0; i < elements.size(); i++) {
				particularElement = elements.get(i);
				if (particularElement.getText().equalsIgnoreCase(elementtoget)) {
					break;
				}
				if(i==22) {
					scrollWindow("LastElementOfNewList_xpath");
				}
			}
		}
		return particularElement;
	}

	public static HashMap<String, String> getData(String FilePath, String SheetName, String TestCase)
			throws IOException {
		int testCaseRowNumber;
		XLSReader xl = new XLSReader(FilePath);
		testCaseRowNumber = xl.testCaseRow(SheetName, TestCase);

		HashMap<String, String> tdata = xl.gettestdata(SheetName, testCaseRowNumber);
		return tdata;
	}

	public void fnLogout() {
		System.out.println("Logout from application ---------------");
		driver.switchTo().defaultContent();
		explicitwait("ProfileImage_xpath", 20, "ElementToBeClickable");
		click("ProfileImage_xpath");
		click("LogoutLink_xpath");
		driver.close();
	}

	public WebElement getElement(String locatorKey) {

		try {

			if (locatorKey.endsWith("_id")) {
				element = driver.findElement(By.id(prop.getProperty(locatorKey)));
			} else if (locatorKey.endsWith("name")) {
				element = driver.findElement(By.name(prop.getProperty(locatorKey)));
			} else if (locatorKey.endsWith("classname")) {
				element = driver.findElement(By.className(prop.getProperty(locatorKey)));
			} else if (locatorKey.endsWith("linktext")) {
				element = driver.findElement(By.linkText(prop.getProperty(locatorKey)));
			} else if (locatorKey.endsWith("_xpath")) {
				element = driver.findElement(By.xpath(prop.getProperty(locatorKey)));
			} else if (locatorKey.endsWith("cssselector")) {
				element = driver.findElement(By.cssSelector(prop.getProperty(locatorKey)));
			} else {
				Assert.fail("The provided Locator - " + locatorKey + " is not found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return element;

	}

	public boolean isElementPresent(String locatorKey) {
		List<WebElement> e = null;
		if (locatorKey.endsWith("id")) {

			e = driver.findElements(By.id(prop.getProperty(locatorKey)));

		} else if (locatorKey.endsWith("name")) {

			e = driver.findElements(By.name(prop.getProperty(locatorKey)));

		} else if (locatorKey.endsWith("classname")) {

			e = driver.findElements(By.className(prop.getProperty(locatorKey)));

		} else if (locatorKey.endsWith("linktext")) {

			e = driver.findElements(By.linkText(prop.getProperty(locatorKey)));

		} else if (locatorKey.endsWith("xpath")) {

			e = driver.findElements(By.xpath(prop.getProperty(locatorKey)));

		} else if (locatorKey.endsWith("cssselector")) {

			e = driver.findElements(By.cssSelector(prop.getProperty(locatorKey)));

		} else {

			Assert.fail("The provided Locator - " + locatorKey + " is not found");
		}

		if (e.size() == 0)
			return false;
		else
			return true;
	}

	public String verifyElementPresent(String locatorKey) {
		boolean result = isElementPresent(locatorKey);
		if (result)
			return Constants.PASS;
		else
			return Constants.FAIL + " - Could not find element " + locatorKey;
	}

	public void input(String locatorKey, String data) {
		// getElement(locatorKey).clear();

		getElement(locatorKey).sendKeys(data);
	}

	public void click(String locatorKey) {
		getElement(locatorKey).click();
	}

	public void clickDynamicElement(String locatorKey, String locaterType) {
		getElementWithReplacementValue(locatorKey, locaterType).click();
	}

	public List<WebElement> getElements(String locatorKey) {
		List<WebElement> element = driver.findElements(By.xpath(prop.getProperty(locatorKey)));
		return element;
	}

	public String getText(String locatorKey) {
		String fieldValue = getElement(locatorKey).getText();
		return fieldValue;
	}

	public String selectDDByValue(String locatorKey, String Value) {
		Select s = new Select(getElement(locatorKey));
		s.selectByValue(Value);
		;
		return Value;
	}

	public void selectDDByVisibleText(String locatorKey, String data) {
		Select s = new Select(getElement(locatorKey));
		s.selectByVisibleText(data);
	}
	
	public void selectByPartialVisibleText(String locatorKey, String expectedText) {
		List<WebElement> options = driver.findElements(By.xpath(prop.getProperty(locatorKey)));

		for (WebElement option : options) {

			if (option.getText().toLowerCase().contains(expectedText.toLowerCase())) {

				Select s = new Select(getElement(locatorKey));
				s.selectByVisibleText(option.getText());

			}
		}
	}
	
	public void selectDDByIndex(String locatorKey, int index) {
		Select s = new Select(getElement(locatorKey));
		s.selectByIndex(index);
	}
	
	

	public void acceptAlert() {
		Alert at = driver.switchTo().alert();
		at.accept();
	}

	public void popupAlert(String locator) throws InterruptedException {

		Thread.sleep(2000);
		switchToFrame("tapestry");
		Thread.sleep(2000);
		if (getElement(locator).isDisplayed()) {
			Thread.sleep(2000);
			click(locator);
		}
	}

	public void scrollWindow(String locator) throws InterruptedException {
		WebElement element = getElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(2000);

	}

	public void highlightElement(String locator) {
		WebElement ele = getElement(locator);
		((JavascriptExecutor) driver)
				.executeScript("argument[0].setAttribute('style','background: yellow; border: 2px solid red;');", ele);

	}

	public void clearValue(String locator) {
		getElement(locator).clear();

	}

	public void AddAddress(String Addressto, String testName, HashMap<String, String> data) throws Exception {
		
		String nameofCurrMethod = new Throwable().getStackTrace()[0].getMethodName();

		
		try {
			click("AddressAdd_xpath");
			if (Addressto.equalsIgnoreCase("Add Residential/Tax") || Addressto.equalsIgnoreCase("Add Mailing")
					|| Addressto.equalsIgnoreCase("Add Temporary")) {
				if (Addressto.equalsIgnoreCase("Add Residential/Tax")) {
					click("AddResidentialTaxAddress_xpath");
				} else if (Addressto.equalsIgnoreCase("Add Mailing")) {
					click("AddMailingAddress_xpath");
				} else if (Addressto.equalsIgnoreCase("Add Temporary")) {
					click("AddTemporaryAddress_xpath");
				}
				Thread.sleep(2000);
				popuphandler();
				Thread.sleep(2000);

				if (!data.get("Res_Zip").isEmpty()) {
					input("Residential/TaxAddress_Zip_xpath", data.get("Res_Zip") + Keys.TAB);
				}
				
				if (!data.get("Res_Address 1").isEmpty()) {
					input("Residential/TaxAddress_Address1_xpath", data.get("Res_Address 1"));
				}
				
				
			
				/*if (!data.get("Res_Address Attention").isEmpty()) {
					input("Residential/TaxAddress_AddressAttention_xpath", data.get("Res_Address Attention"));
				}			
	
				if (!data.get("Res_Address 2").isEmpty()) {
					input("Residential/TaxAddress_Address2_xpath", data.get("Res_Address 2"));
				}
	
				if (!data.get("Res_Address 3").isEmpty()) {
					input("Residential/TaxAddress_Address3_xpath", data.get("Res_Address 3"));
				}*/

				if (!data.get("Res_Start Date").isEmpty()) {
					getElement("Residential/TaxAddress_StartDate_xpath").clear();
					input("Residential/TaxAddress_StartDate_xpath", data.get("Res_Start Date"));
				}
			}
			Thread.sleep(2000);
			switchToDiv("Residential/TaxAddressOk_xpath");
			Thread.sleep(2000);
			logger.log(LogStatus.PASS, "Address filled successfully");
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Error in Address filling", testName, nameofCurrMethod);
		}

	}

	public void fnSearch(String testname, String sheetname) throws IOException, InterruptedException {

		System.out.println("Search annuitant");
		driver.switchTo().defaultContent();
		HashMap<String, String> data = getData(Constants.INPUT_XLS, sheetname, testname);
		getElement("searchBox_xpath").clear();
		input("searchBox_xpath", data.get("SSN"));
		click("searchButton_xpath");

	}

	public void loginWithDifferentUserID(String testname) throws Exception {

		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		Thread.sleep(2000);
		fnLogout();
		Thread.sleep(5000);
		//clearBrowserCache();
		Login logobj = new Login();
		logobj.login(testname, "Approver_ID");
		logger.log(LogStatus.INFO, "logging with Approver");
		Thread.sleep(2000);

	}

	public boolean compareStrings(String string1, String string2) {

		if (string1.equals(string2)) {
			return true;
		} else {
			return false;
		}
	}

	// ################# ------- Dhanraj -------- ################################

	public WebElement getElementWithReplacementValue(String locatorKey, String locaterType) {

		try {

			if (locaterType.equals("id")) {
				element = driver.findElement(By.id(locatorKey));
			} else if (locaterType.equals("name")) {
				element = driver.findElement(By.name(locatorKey));
			} else if (locaterType.equals("classname")) {
				element = driver.findElement(By.className(locatorKey));
			} else if (locaterType.equals("linktext")) {
				element = driver.findElement(By.linkText(locatorKey));
			} else if (locaterType.equals("xpath")) {
				element = driver.findElement(By.xpath(locatorKey));
			} else if (locaterType.equals("cssselector")) {
				element = driver.findElement(By.cssSelector(locatorKey));
			} else {
				Assert.fail("The provided Locator - " + locatorKey + " is not found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return element;

	}

	public void assertEquals(String actual, String expected) {
		Assert.assertEquals(actual, expected);
	}

	public void assertNotEquals(String actual, String expected) {
		Assert.assertNotEquals(actual, expected);
	}

	public void assertTrue(boolean condition) {
		Assert.assertTrue(condition);

	}

	public String foaShortName(String foaName) {

		String shortName = "";

		switch (foaName) {
		case "Temporary Life Annuity":
			shortName = "TLA";
			break;

		case "Life Annuity":
			shortName = "LIFE";
			break;
		case "Lump Sum":
			shortName = "LIFE";
			break;

		case "Term Certain Only Annuity":
			shortName = "TC";
			break;

		default:
			System.out.println("Invalid grade");
		}
		return shortName;
	}

	// Method use replace value string to string
	public String replacementInLocator(String locator, String replaceValue) {

		String currentLocator = prop.getProperty(locator);
		String updatedLocator = currentLocator.replace("$replacement-value$", replaceValue);
		return updatedLocator;
	}

	public List<WebElement> getElementsWithReplacementValue(String locatorKey) {
		List<WebElement> element = driver.findElements(By.xpath(locatorKey));
		return element;
	}

	// search from home page
	public void search(String locator, String data) throws InterruptedException {
		input(locator, data + "\n");
		Thread.sleep(2000);
	}

	// Get alternate ID from sheet
	public String annuitantAltId(String sheetName, String testName) throws IOException {
		HashMap<String, String> annuitantData = getData(Constants.INPUT_XLS, sheetName, testName);
		String altId = annuitantData.get("Alternate ID");
		return altId;
	}

	// search alternateID and click on edit
	public void searchAnnuitantAltIdAndClickEdit(String sheetName, String testName)throws InterruptedException, IOException {
		driver.switchTo().defaultContent();
		clearValue("Search_xpath");
		search("Search_xpath", annuitantAltId("Annuitant", testName));
		Thread.sleep(2000);
		switchToDiv("Edit_xpath");
		Thread.sleep(2000);

	}

	// Click on OK when Popup come
	public void popupError() throws Exception {

		if (!isElementPresent("ErrorOkButton_xpath")) {
			System.out.println("main page");
		} else {
			System.out.println("popup");
			enterKey();
		}
	}
	
	// method is use to press enter of keyboard
	public static void enterKey() throws Exception
    {
		Actions action = new Actions(driver);
		 action.sendKeys(Keys.ENTER).build().perform();	
    }

	public static void homeKey() throws Exception{
		Actions action =new Actions(driver);
		action.sendKeys(Keys.HOME).build().perform();
	}

}