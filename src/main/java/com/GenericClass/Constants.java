package main.java.com.GenericClass;

public class Constants {
	public static final String BUSINESSFLOW_XLS = System.getProperty("user.dir")+"\\data\\SS_Admin_Business_Flow.xlsx";
	public static final String INPUT_XLS = System.getProperty("user.dir")+"\\data\\SS_Admin_Input_Consolidated.xlsx";
	public static final String OUTPUT_XLS = System.getProperty("user.dir")+"\\output\\TC_581.xlsx";
	public static final String MICRO_XLS = System.getProperty("user.dir")+"\\output\\Revised Validation Template.xlsm";
	
	public static final String PASS = "Pass";
	public static final String FAIL = "Fail";
	public static final int WAIT = 20;	
	public static String FieldValue = null;
	public static String lockInNumber = "211399";

}
