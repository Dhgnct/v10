package main.java.com.TCDriver;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class TC_driver_BrokerValidatoin extends GenericClass {
	
	public Login loginObj = null;
	public Person personObj = null;
	public Annuitant annuitantObj = null;
	public ContractsAndCerts contractsAndCertsObj = null;
	public BenefitsPage benefitsObj = null;
	public PaymentHistory paymentHistoryObj = null;
	public TransactionPage transactionPageObj = null;
	public PaymentSetupPage paymentSetupPageObj = null;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		loginObj = PageFactory.initElements(driver, Login.class);
		personObj = PageFactory.initElements(driver, Person.class);
		annuitantObj = PageFactory.initElements(driver, Annuitant.class);
		contractsAndCertsObj = PageFactory.initElements(driver, ContractsAndCerts.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		transactionPageObj = PageFactory.initElements(driver, TransactionPage.class);
		paymentSetupPageObj = PageFactory.initElements(driver, PaymentSetupPage.class);
	}

	@Test
	public void CI_TC_8() throws Exception {

		
		logger = reports.startTest("CI_TC_8", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_8", "Processor_ID");

		annuitantObj.createAnnuitant("CI_TC_8");
		contractsAndCertsObj.addBroker("CI_TC_8");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_8");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_8");
		transactionPageObj.annuitantARClosed("CI_TC_8");
		contractsAndCertsObj.brokerCommissionValidation("CI_TC_8");
	}

	@Test
	public void CI_TC_9() throws Exception {

		logger = reports.startTest("CI_TC_9",
				"Broker commissoin workflow Validatoin with 2 broker having commission split and production split percentage");
		
		loginObj.login("CI_TC_9", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_9");
		contractsAndCertsObj.addBroker("CI_TC_9");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_9");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_9");
		transactionPageObj.annuitantARClosed("CI_TC_9");
		contractsAndCertsObj.brokerCommissionValidation("CI_TC_9");
	}
	
	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}
}
