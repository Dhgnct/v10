package main.java.com.TCDriver;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class TC_driver_Relationship extends GenericClass {
	
	public Login loginObj = null;
	public Person personObj = null;
	public Annuitant annuitantObj = null;
	public ContractsAndCerts contractsAndCertsObj = null;
	public BenefitsPage benefitsObj = null;
	public PaymentHistory paymentHistoryObj = null;
	public TransactionPage transactionPageObj = null;
	public PaymentSetupPage paymentSetupPageObj = null;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		loginObj = PageFactory.initElements(driver, Login.class);
		personObj = PageFactory.initElements(driver, Person.class);
		annuitantObj = PageFactory.initElements(driver, Annuitant.class);
		contractsAndCertsObj = PageFactory.initElements(driver, ContractsAndCerts.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		transactionPageObj = PageFactory.initElements(driver, TransactionPage.class);
		paymentSetupPageObj = PageFactory.initElements(driver, PaymentSetupPage.class);
	}

	@Test
	public void CI_TC_124() throws Exception {

		logger = reports.startTest("CI_TC_124", "Verify that Joint Annuitant can be added  as relationship");
		
		loginObj.login("CI_TC_124", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_124");
		contractsAndCertsObj.addRelationShip("CI_TC_124");
		contractsAndCertsObj.addBroker("CI_TC_124");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_124");
		
	}
	
	@Test
	public void CI_TC_125() throws Exception {

		logger = reports.startTest("CI_TC_125", "Verify that Joint Annuitant can be Modified as relationship");
		
		loginObj.login("CI_TC_125", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_125");
		contractsAndCertsObj.addRelationShip("CI_TC_125");
		contractsAndCertsObj.addBroker("CI_TC_125");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_125");
		contractsAndCertsObj.updateRelationshipFirstName("CI_TC_125");
	}
	
	@Test
	public void CI_TC_126() throws Exception {

		logger = reports.startTest("CI_TC_126", "Verify that Joint Annuitant can be stopped as relationship");
		
		loginObj.login("CI_TC_125", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_126");
		contractsAndCertsObj.addRelationShip("CI_TC_126");
		contractsAndCertsObj.addBroker("CI_TC_126");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_126");
		contractsAndCertsObj.updateRelationshipStopDate("CI_TC_126");
	}
	
	@Test
	public void CI_TC_128() throws Exception {

		logger = reports.startTest("CI_TC_126", "Verify that Joint Annuitant can be stopped as relationship");
		
		loginObj.login("CI_TC_128", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_128");
		contractsAndCertsObj.addRelationShip("CI_TC_128");
		contractsAndCertsObj.addBroker("CI_TC_128");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_128");
		contractsAndCertsObj.updateRelationshipFirstName("CI_TC_128");
	}
	
	
	
	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}
}
