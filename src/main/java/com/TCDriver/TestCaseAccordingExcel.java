package main.java.com.TCDriver;

import org.apache.poi.ss.usermodel.Sheet;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.GenericClass.XLSReader;

public class TestCaseAccordingExcel extends GenericClass {

	Sheet sheet = null;
	XLSReader xl = new XLSReader(Constants.BUSINESSFLOW_XLS); 

	Methods method = new Methods();
	int batchCount;
	int nextExecutionColNum;
	int batchValuecount;
	boolean batchExist;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		GenericClass.GenericClassMethod();
		GenericClass.deletePreviousReportingFolder();
		GenericClass.reporting();
	}

	@Test
	public void initialMethod() throws Exception {

		int totalRowCount = xl.getRowCount("Demo");
		System.out.println("----------totalRowCount------------" + totalRowCount);

		// Row wise iteration
		for (int rowNum = 2; rowNum <= totalRowCount; rowNum++) {

			batchExist = false;
			batchCount = 0;
			nextExecutionColNum = 0;
			batchValuecount = 0;
			String executeValue = xl.getCellData("Demo", "Execute", rowNum); // getting Y or N value of Execute
			if (executeValue.equalsIgnoreCase("Y")) {

				String batchValue = xl.getCellData("Demo", "Batch", rowNum); // getting count of batch
				String TC_ID = xl.getCellData("Demo", "TC_ID", rowNum); // getting test case id
				// condition check batch value is float or integer
				if (batchValue.contains(".")) { // condition of floating value having . in value
					batchValue = batchValue.substring(0, batchValue.indexOf('.')); // removing fractional part
					batchValuecount = Integer.parseInt(batchValue); // converting batchValue into integer
				} else {
					batchValuecount = Integer.parseInt(batchValue);
				}
				if (batchValuecount == 0) {

					System.out.println("Value of functionStatus----" + functionStatus);
					// column wise iteration for pick method from cell
					for (int colNum = 4; colNum < xl.getColumnCount("Demo"); colNum++) {

						System.out.println("col wise function status vlaue===" + functionStatus);
						if (colNum == 4) {
							functionStatus = "true";
							System.out.println("function status value when new start=" + functionStatus);
							logger = reports.startTest(TC_ID);
							method.differentMethods("login", TC_ID);
							xcellConditions("Demo", colNum, rowNum);
						} else {
							if (functionStatus != "fail") {

								if (batchExist == true) {
									break;
								} else {
									xcellConditions("Demo", colNum, rowNum);
								}
							} else {
								driver.close();
								break;
							}
						}
					}
				}
						
						
						
						
						
						
						/*if (functionStatus != false) {

							if (batchExist == true) { // batchExist is true when batch fetch from excel
								break; // break column wise iteration
							} else {
								if (colNum == 4) { // If match first login then execute method
									logger = reports.startTest(TC_ID);
									functionStatus = true;
									method.differentMethods("login", TC_ID); // First login before method execution
									xcellConditions("Demo", colNum, rowNum); // Call method
								} else {
									xcellConditions("Demo", colNum, rowNum);
								}
							}
						} else {
							break;
						}
					}
				}*/ else {

					// Column wise iteration for counting the batch count
					for (int colNum = 1; colNum < xl.getColumnCount("Demo"); colNum++) {

						batchValue = xl.getCellData("Demo", colNum, rowNum);
						if ((batchValue.equalsIgnoreCase("fnPremium") || batchValue.equalsIgnoreCase("batch"))) {
							batchCount = batchCount + 1; // Increasing batch value with 1
							if (batchCount == batchValuecount) { // Counting how many time batch or fnPremiunm occur
								nextExecutionColNum = colNum + 1; // Next execution start column is store
								break;
							}
						}
					}

					// Column wise iteration for pick method from cell
					for (int colNum = nextExecutionColNum; colNum < xl.getColumnCount("Demo"); colNum++) {

						if (colNum == nextExecutionColNum) {
							functionStatus = "true";
							method.differentMethods("login", TC_ID);
							xcellConditions("Demo", colNum, rowNum);
						} else {
							if (functionStatus != "fail") {
								if (batchExist == true) { // batchExist is true when batch fetch from excel
									break; // break column wise iteration
								} else { // Call method
									xcellConditions("Demo", colNum, rowNum);
								}
								
							}
						}
					}
				}
			}
		}
	}

	// Method for different condition in excel
	public void xcellConditions(String sheetName, int colNum, int rowNum) throws Exception {

		String methodName = xl.getCellData(sheetName, colNum, rowNum);
		if (methodName.equals("")) {
			xl.setCellData(sheetName, "Execute", rowNum, "C");
		} else if (methodName.equalsIgnoreCase("fnPremium") || methodName.equalsIgnoreCase("batch")) {
			String updatedBatchValue = Integer.toString(batchValuecount + 1); // Converting integer to string
			xl.setCellData("Demo", "Batch", rowNum, updatedBatchValue); // Update batch value in sheet
			// System.exit(1); // successfully termination of test
			batchExist = true;
			driver.close(); // Closing the window
		} else {
			String testCase = xl.getCellData("Demo", 1, rowNum); // Getting test case name
			method.differentMethods(methodName, testCase); // Call method of getting from cell
		}
}

	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}

}