package main.java.com.TCDriver;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.FactoringModellingToolPage;
import main.java.com.SSA_Pages.FactoringPage;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.Quote_Revision;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class TC_driver_QuoteRevision extends GenericClass {

	public static String className;
	public Login loginObj = null;
	public Person personObj = null;
	public Annuitant annuitantObj = null;
	public ContractsAndCerts contractsAndCertsObj = null;
	public BenefitsPage benefitsObj = null;
	public PaymentHistory paymentHistoryObj = null;
	public TransactionPage transactionPageObj = null;
	public PaymentSetupPage paymentSetupPageObj = null;
	public Quote_Revision quoteRevisionObj = null;
	public FactoringPage factoringObj = null;
	public FactoringModellingToolPage factModellingObj = null;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		loginObj = PageFactory.initElements(driver, Login.class);
		personObj = PageFactory.initElements(driver, Person.class);
		annuitantObj = PageFactory.initElements(driver, Annuitant.class);
		contractsAndCertsObj = PageFactory.initElements(driver, ContractsAndCerts.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		transactionPageObj = PageFactory.initElements(driver, TransactionPage.class);
		paymentSetupPageObj = PageFactory.initElements(driver, PaymentSetupPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		quoteRevisionObj = PageFactory.initElements(driver, Quote_Revision.class);
		factoringObj = PageFactory.initElements(driver, FactoringPage.class);
		factModellingObj = PageFactory.initElements(driver, FactoringModellingToolPage.class);
		GenericClass.GenericClassMethod();
		GenericClass.deletePreviousReportingFolder();
		GenericClass.reporting();
		
	}
	
	/*@Test
	public void CI_TC_104() throws Exception {
			
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
			
		loginObj.login("CI_TC_104", "Processor_ID");
		//factoringObj.createAndAddMultipleFactoringCompany("CI_TC_104");
		factModellingObj.FactoringModelingUIOrganisationTableData("CI_TC_104", "Open");		
		}*/
	
		
	
	@Test
	public void CI_TC_10() throws Exception {
		
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
		
		loginObj.login("CI_TC_10", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_10");
		contractsAndCertsObj.addRelationShip("CI_TC_10");
		contractsAndCertsObj.addBroker("CI_TC_10");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_10");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_10");		
		transactionPageObj.annuitantARClosed("CI_TC_10");
		benefitsObj.caseActivationPFSteps("CI_TC_10");
		
		//quoteRevisionObj.createQuoteRevision("CI_TC_10");
		
	}
	

	//@Test
	/*public void CI_TC_10() throws Exception {
		//className = this.getClass().getName();
			
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
		
		loginObj.login("CI_TC_10", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_10");
		//contractsAndCertsObj.addRelationShip("TC_581", "Joint Ann", "Beneficiary");
		contractsAndCertsObj.addBroker("CI_TC_10");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_10");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_10");
				
		quoteRevisionObj.createQuoteRevision("CI_TC_10");
		
	}*/
	
		
	//@Test
	/*public void CI_TC_11() throws Exception {
		className = this.getClass().getName();
				
		//System.out.println("class Name in ci_tc_10 method" + className);
		
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
		
		loginObj.login("CI_TC_11", "Processor_ID");
				
	}*/
		
	//@Test
	/*public void CI_TC_16() throws Exception {
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
		
		loginObj.login("CI_TC_10", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_11");
		//contractsAndCertsObj.addRelationShip("TC_581", "Joint Ann", "Beneficiary");
		contractsAndCertsObj.addBroker("CI_TC_11");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_11");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_11");
				
		//quoteRevisionObj.createQuoteRevision("CI_TC_11");
		
	}*/
	
	/*public void CI_TC_12() throws Exception {
		logger = reports.startTest("Test- 1 : Login Page", "Login test");
		loginObj.login("TC_581", "Processor_ID");

		annuitantObj.createAnnuitant("TC_590");
		contractsAndCertsObj.addRelationShip("TC_590");
		contractsAndCertsObj.addBroker("TC_590");
		
		// paymentHistoryObj.createPaymentHistory("TC_581"); //Payment History - Option
				// Stop
				// paymentHistoryObj.createPaymentHistory("ADMIN_TC_2"); //Payment History -
				// Option Void
				// overPaymentsObj.createOverPayments("TC_582");
				// overPaymentsDeductObj.createOverPaymentDeductions("TC_581");
		
		// benefitsObj.IncreaseFOAAmount("TC_581");
				// overPaymentsWriteofObj.createOverPaymentsWriteOff("TC_581");

	}*/

	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}
}
