package main.java.com.TCDriver;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class Methods extends GenericClass{

	public void differentMethods(String methodName, String testname) throws Exception {
		
		
		Login loginObj = new Login();
		Person personObj = null;
		Annuitant annuitantObj = new Annuitant();
		ContractsAndCerts contractsAndCertsObj = new ContractsAndCerts();
		BenefitsPage benefitsObj = new BenefitsPage();
		PaymentHistory paymentHistoryObj = new PaymentHistory();
		TransactionPage transactionPageObj = new TransactionPage();
		PaymentSetupPage paymentSetupPageObj = new PaymentSetupPage();
		
		
		
		//Test test = new Test();
		
		/*benefitsObj.benefitsHeaderAndSsDetails("TC_590");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("TC_590");
		transactionPageObj.annuitantARClosed("TC_590");
		benefitsObj.caseActivationPFSteps("TC_590");

		paymentSetupPageObj.AddPaymentDeduction("TC_590");
		benefitsObj.caseActivationPFSteps("TC_590");
		paymentHistoryObj.createPaymentHistory("TC_590");
		transactionPageObj.incomingARclosed("TC_583");

		annuitantObj.executedDeathAndVerification("TC_590");
		benefitsObj.addSecondaryApplication("TC_590");
		benefitsObj.secondaryAppPfSteps("TC_590");*/
		
		switch (methodName) {
		
		case "login":
			loginObj.login(testname, "Processor_ID");
			break;
			
		case "createAnnuitant":
			annuitantObj.createAnnuitant(testname);
			break;
			
		case "addRelationShip":
			contractsAndCertsObj.addRelationShip(testname);
			break;
			
		case "addBroker":
			contractsAndCertsObj.addBroker(testname);
			break;
			
		case "benefitsHeaderAndSsDetails":
			benefitsObj.benefitsHeaderAndSsDetails(testname);
			break;
			
		case "pfSteps_ValidateBroker_GeanerateLokin":
			contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin(testname);
			break;
			
		case "annuitantARClosed":
			transactionPageObj.annuitantARClosed(testname);
			break;
			
		case "caseActivationPFSteps":
			benefitsObj.caseActivationPFSteps(testname);
			break;
			
		/*case "test1":
			test.Test1();
			break;
			
		case "test2":
			test.Test2();
			break;
			
		case "test3":
			test.Test3();
			break;
			
		case "test4":
			test.Test4();
			break;
			
		case "test5":
			test.Test5();
			break;*/

		default:
			System.out.println("-------------Called method is not in switch case------------");
			break;
		}

	}
}
