package main.java.com.TCDriver;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class TC_driver_PaymentMethod extends GenericClass {
	
	public Login loginObj = null;
	public Person personObj = null;
	public Annuitant annuitantObj = null;
	public ContractsAndCerts contractsAndCertsObj = null;
	public BenefitsPage benefitsObj = null;
	public PaymentHistory paymentHistoryObj = null;
	public TransactionPage transactionPageObj = null;
	public PaymentSetupPage paymentSetupPageObj = null;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		loginObj = PageFactory.initElements(driver, Login.class);
		personObj = PageFactory.initElements(driver, Person.class);
		annuitantObj = PageFactory.initElements(driver, Annuitant.class);
		contractsAndCertsObj = PageFactory.initElements(driver, ContractsAndCerts.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		transactionPageObj = PageFactory.initElements(driver, TransactionPage.class);
		paymentSetupPageObj = PageFactory.initElements(driver, PaymentSetupPage.class);
		GenericClass.GenericClassMethod();
		GenericClass.deletePreviousReportingFolder();
		GenericClass.reporting();
	}
	
	@Test(enabled = true)
	public void CI_TC_145() throws Exception {

		
		logger = reports.startTest("CI_TC_145", "Broker commission workflow Validation with single broker");
		loginObj.login("CI_TC_145", "Processor_ID");

		personObj.createPerson("CI_TC_145");
		annuitantObj.createAnnuitant("CI_TC_145");
		/*contractsAndCertsObj.addBroker("CI_TC_145");
		contractsAndCertsObj.addRelationShip("CI_TC_145");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_145");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_145");
		transactionPageObj.annuitantARClosed("CI_TC_145");
		benefitsObj.caseActivationPFSteps("CI_TC_145");*/
	}
	
	
	@Test(enabled = false)
	public void CI_TC_146() throws Exception {

		
		logger = reports.startTest("CI_TC_146", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_146", "Processor_ID");

		//personObj.createPerson("CI_TC_146");
		//annuitantObj.createAnnuitant("CI_TC_146");
		//contractsAndCertsObj.addBroker("CI_TC_146");
		//contractsAndCertsObj.addRelationShip("CI_TC_146");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_146");
		//contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_146");
		//transactionPageObj.annuitantARClosed("CI_TC_146");
		//benefitsObj.caseActivationPFSteps("CI_TC_146");
		//paymentSetupPageObj.updatePaymentmethod("CI_TC_146");
	}
	
	@Test(enabled = false)
	public void CI_TC_147() throws Exception {

		
		logger = reports.startTest("CI_TC_147", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_147", "Processor_ID");

		personObj.createPerson("CI_TC_147");
		annuitantObj.createAnnuitant("CI_TC_147");
		contractsAndCertsObj.addBroker("CI_TC_147");
		contractsAndCertsObj.addRelationShip("CI_TC_147");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_147");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_147");
		transactionPageObj.annuitantARClosed("CI_TC_147");
		benefitsObj.caseActivationPFSteps("CI_TC_147");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_147");
	}	
	
	@Test(enabled = false)
	public void CI_TC_148() throws Exception {

		
		logger = reports.startTest("CI_TC_148", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_148", "Processor_ID");

		/*personObj.createPerson("CI_TC_148");
		annuitantObj.createAnnuitant("CI_TC_148");
		contractsAndCertsObj.addBroker("CI_TC_148");*/
		contractsAndCertsObj.addRelationShip("CI_TC_148");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_148");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_148");
		transactionPageObj.annuitantARClosed("CI_TC_148");
		benefitsObj.caseActivationPFSteps("CI_TC_148");
		paymentSetupPageObj.QDROCaseactivation("CI_TC_148");
		//paymentSetupPageObj.updatePaymentmethod("CI_TC_147");
	}
	
	/*@Test(enabled = false)
	public void CI_TC_149() throws Exception {

		
		logger = reports.startTest("CI_TC_149", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_149", "Processor_ID");

		personObj.createPerson("CI_TC_149");
		annuitantObj.createAnnuitant("CI_TC_149");
		contractsAndCertsObj.addBroker("CI_TC_149");
		contractsAndCertsObj.addRelationShip("CI_TC_149");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_149");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_149");
		transactionPageObj.annuitantARClosed("CI_TC_149");
		benefitsObj.caseActivationPFSteps("CI_TC_149");
		paymentSetupPageObj.QDROCaseactivation("CI_TC_149");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_149");
		//paymentSetupPageObj.updatePaymentmethod("CI_TC_149");
	}*/
	
	@Test(enabled = false)
	public void CI_TC_150() throws Exception {

		
		logger = reports.startTest("CI_TC_150", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_150", "Processor_ID");

		personObj.createPerson("CI_TC_150");
		annuitantObj.createAnnuitant("CI_TC_150");
		contractsAndCertsObj.addBroker("CI_TC_150");
		contractsAndCertsObj.addRelationShip("CI_TC_150");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_150");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_150");
		transactionPageObj.annuitantARClosed("CI_TC_150");
		benefitsObj.caseActivationPFSteps("CI_TC_150");
		paymentSetupPageObj.QDROCaseactivation("CI_TC_150");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_150");
		paymentSetupPageObj.deletePaymentmethod("CI_TC_150");
	}
	
	@Test(enabled = false)
	public void CI_TC_151() throws Exception {

		
		logger = reports.startTest("CI_TC_151", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_151", "Processor_ID");

		personObj.createPerson("CI_TC_151");
		annuitantObj.createAnnuitant("CI_TC_151");
		contractsAndCertsObj.addBroker("CI_TC_151");
		contractsAndCertsObj.addRelationShip("CI_TC_151");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_151");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_151");
		transactionPageObj.annuitantARClosed("CI_TC_151");
		benefitsObj.caseActivationPFSteps("CI_TC_151");
		
	}
	
	@Test(enabled = false)
	public void CI_TC_152() throws Exception {

		
		logger = reports.startTest("CI_TC_152", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_152", "Processor_ID");

		personObj.createPerson("CI_TC_152");
		annuitantObj.createAnnuitant("CI_TC_152");
		contractsAndCertsObj.addBroker("CI_TC_152");
		contractsAndCertsObj.addRelationShip("CI_TC_152");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_152");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_152");
		transactionPageObj.annuitantARClosed("CI_TC_152");
		benefitsObj.caseActivationPFSteps("CI_TC_152");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_152");
		paymentSetupPageObj.deletePaymentmethod("CI_TC_152");
	}
	
	@Test(enabled = false)
	public void CI_TC_153() throws Exception {

		
		logger = reports.startTest("CI_TC_153", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_153", "Processor_ID");

		personObj.createPerson("CI_TC_153");
		annuitantObj.createAnnuitant("CI_TC_153");
		contractsAndCertsObj.addBroker("CI_TC_153");
		contractsAndCertsObj.addRelationShip("CI_TC_153");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_153");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_153");
		transactionPageObj.annuitantARClosed("CI_TC_153");
		benefitsObj.caseActivationPFSteps("CI_TC_153");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_153");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_153");
	}
	
	@Test(enabled = false)
	public void CI_TC_154() throws Exception {

		
		logger = reports.startTest("CI_TC_154", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_154", "Processor_ID");

		personObj.createPerson("CI_TC_154");
		annuitantObj.createAnnuitant("CI_TC_154");
		contractsAndCertsObj.addBroker("CI_TC_154");
		contractsAndCertsObj.addRelationShip("CI_TC_154");
		/*benefitsObj.benefitsHeaderAndSsDetails("CI_TC_154");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_154");
		transactionPageObj.annuitantARClosed("CI_TC_154");
		benefitsObj.caseActivationPFSteps("CI_TC_154");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_154");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_154");*/
	}
	
	@Test(enabled = false)
	public void CI_TC_155() throws Exception {

		
		logger = reports.startTest("CI_TC_155", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_155", "Processor_ID");

		personObj.createPerson("CI_TC_155");
		annuitantObj.createAnnuitant("CI_TC_155");
		contractsAndCertsObj.addBroker("CI_TC_155");
		contractsAndCertsObj.addRelationShip("CI_TC_155");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_155");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_155");
		transactionPageObj.annuitantARClosed("CI_TC_155");
		benefitsObj.caseActivationPFSteps("CI_TC_153");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_153");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_153");
	}
	
	@Test(enabled = false)
	public void CI_TC_156() throws Exception {

		
		logger = reports.startTest("CI_TC_156", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_156", "Processor_ID");

		personObj.createPerson("CI_TC_156");
		annuitantObj.createAnnuitant("CI_TC_156");
		contractsAndCertsObj.addBroker("CI_TC_156");
		contractsAndCertsObj.addRelationShip("CI_TC_156");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_156");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_156");
		transactionPageObj.annuitantARClosed("CI_TC_156");
		benefitsObj.caseActivationPFSteps("CI_TC_156");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_156");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_156");
	}
	
	@Test(enabled = false)
	public void CI_TC_157() throws Exception {

		
		logger = reports.startTest("CI_TC_157", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_157", "Processor_ID");

		personObj.createOrganisation("CI_TC_157");
		/*annuitantObj.createAnnuitant("CI_TC_157");
		contractsAndCertsObj.addBroker("CI_TC_157");
		contractsAndCertsObj.addRelationShip("CI_TC_157");*/
		/*benefitsObj.benefitsHeaderAndSsDetails("CI_TC_157");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_157");
		transactionPageObj.annuitantARClosed("CI_TC_157");
		benefitsObj.caseActivationPFSteps("CI_TC_157");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_157");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_157");*/
	}
	
	@Test(enabled = false)
	public void CI_TC_158() throws Exception {

		
		logger = reports.startTest("CI_TC_158", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_158", "Processor_ID");

		personObj.createOrganisation("CI_TC_158");
		annuitantObj.createAnnuitant("CI_TC_158");
		contractsAndCertsObj.addBroker("CI_TC_158");
		contractsAndCertsObj.addRelationShip("CI_TC_158");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_158");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_158");
		transactionPageObj.annuitantARClosed("CI_TC_158");
		benefitsObj.caseActivationPFSteps("CI_TC_158");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_158");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_158");
	}
	
	@Test(enabled = false)
	public void CI_TC_159() throws Exception {

		
		logger = reports.startTest("CI_TC_159", "Broker commissoin workflow Validatoin with single broker");
		loginObj.login("CI_TC_159", "Processor_ID");

		personObj.createOrganisation("CI_TC_159");
		annuitantObj.createAnnuitant("CI_TC_159");
		contractsAndCertsObj.addBroker("CI_TC_159");
		contractsAndCertsObj.addRelationShip("CI_TC_159");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_159");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_159");
		transactionPageObj.annuitantARClosed("CI_TC_159");
		benefitsObj.caseActivationPFSteps("CI_TC_159");
		paymentSetupPageObj.AddPaymentmethod("CI_TC_159");
		paymentSetupPageObj.updatePaymentmethod("CI_TC_159");
	}
	
	
	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}

}


