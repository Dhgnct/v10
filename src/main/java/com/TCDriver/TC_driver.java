package main.java.com.TCDriver;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import main.java.com.GenericClass.GenericClass;
import main.java.com.SSA_Pages.Annuitant;
import main.java.com.SSA_Pages.BenefitsPage;
import main.java.com.SSA_Pages.ContractsAndCerts;
import main.java.com.SSA_Pages.PaymentHistory;
import main.java.com.SSA_Pages.PaymentSetupPage;
import main.java.com.SSA_Pages.Person;
import main.java.com.SSA_Pages.TransactionPage;
import main.java.com.login.Login;

public class TC_driver extends GenericClass {

	public Login loginObj = null;
	public Person personObj = null;
	public Annuitant annuitantObj = null;
	public ContractsAndCerts contractsAndCertsObj = null;
	public BenefitsPage benefitsObj = null;
	public PaymentHistory paymentHistoryObj = null;
	public TransactionPage transactionPageObj = null;
	public PaymentSetupPage paymentSetupPageObj = null;

	@BeforeSuite
	public void AppLauncher() throws Exception {

		System.out.println("Please wait while system launch browser for testing.......");
		loginObj = PageFactory.initElements(driver, Login.class);
		personObj = PageFactory.initElements(driver, Person.class);
		annuitantObj = PageFactory.initElements(driver, Annuitant.class);
		contractsAndCertsObj = PageFactory.initElements(driver, ContractsAndCerts.class);
		benefitsObj = PageFactory.initElements(driver, BenefitsPage.class);
		paymentHistoryObj = PageFactory.initElements(driver, PaymentHistory.class);
		transactionPageObj = PageFactory.initElements(driver, TransactionPage.class);
		paymentSetupPageObj = PageFactory.initElements(driver, PaymentSetupPage.class);
		GenericClass.GenericClassMethod();
		GenericClass.reporting();
		
	}

	@Test(priority = 1)
	public void Login() throws Exception {
		logger = reports.startTest("CI_TC_11 : ", "End to End");
		loginObj.login("CI_TC_11", "Processor_ID");
		annuitantObj.createAnnuitant("CI_TC_11");
		contractsAndCertsObj.addRelationShip("CI_TC_11");
		contractsAndCertsObj.addBroker("CI_TC_11");
		benefitsObj.benefitsHeaderAndSsDetails("CI_TC_11");
		contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("CI_TC_11");
		transactionPageObj.annuitantARClosed("CI_TC_11");
		benefitsObj.caseActivationPFSteps("CI_TC_11");	
	}
	
	/*@Test(priority = 1)
	public void Login1() throws Exception {
		logger = reports.startTest("CI_TC_10 : ", "End to End");
		loginObj.login("CI_TC_10", "Processor_ID");	
	}
*/
	@AfterClass
	public static void endTest() {
		reports.endTest(logger);
		reports.flush();
	}
}




/*@Test(priority = 1)
public void Login1() throws Exception {
	logger = reports.startTest("Test- 1 : Login Page", "Login test");
	loginObj.login("TC_581", "Processor_ID");

	
	 * annuitantObj.createAnnuitant("TC_590");
	 * contractsAndCertsObj.addRelationShip("TC_590", "Joint Ann",
	 * "Joint Annuitant"); contractsAndCertsObj.addBroker("TC_590");
	 * benefitsObj.benefitsHeaderAndSsDetails("TC_590");
	 * contractsAndCertsObj.pfSteps_ValidateBroker_GeanerateLokin("TC_590");
	 * transactionPageObj.annuitantARClosed("TC_590");
	 * benefitsObj.caseActivationPFSteps("TC_590");
	 

	// paymentSetupPageObj.AddPaymentDeduction("TC_590");
	// benefitsObj.caseActivationPFSteps("TC_590");
	// paymentHistoryObj.createPaymentHistory("TC_590");
	// transactionPageObj.incomingARclosed("TC_583");

	
	 * annuitantObj.executedDeathAndVerification("TC_590");
	 * benefitsObj.addSecondaryApplication("TC_590");
	 * benefitsObj.secondaryAppPfSteps("TC_590");
	 

}*/