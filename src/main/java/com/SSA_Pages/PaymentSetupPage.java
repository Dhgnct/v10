package main.java.com.SSA_Pages;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class PaymentSetupPage extends GenericClass {

	public void AddPaymentDeduction(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("benefitsLink_xpath");
			Thread.sleep(2000);
			click("benefitsLink_xpath");
			System.out.println("Benefits segement Clicked once");
			Thread.sleep(2000);
			click("Edit_xpath");
			switchToFrame("tapestry");
			click("PaymentSetupTab_xpath");
			Thread.sleep(2000);
			click("PaymentStreamExpand_xpath");
			Thread.sleep(2000);
			addLienandLevies(testName);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Payment deduction not added successfully", testName, nameofCurrMethod);
		}
	}

	public void addLienandLevies(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			HashMap<String, String> addleinandlevydata = getData(Constants.INPUT_XLS, "Ann Liens and Levies", testName);
			Thread.sleep(5000);
			scrollWindow("LienandLevies_xpath");
			click("CogweelLien_xpath");
			click("Add_xpath");
			Thread.sleep(2000);
			popuphandler();
			switchToDiv("Deductiontype_xpath");
			click("Deductiontype_xpath");
			input("Deductiontype_xpath", addleinandlevydata.get("Deduction Type1") + Keys.TAB);
			Thread.sleep(2000);
			input("ThirdPartyPayment_xpath", addleinandlevydata.get("Third Party Payment1"));
			Thread.sleep(2000);
			input("AdditionalPayableInfo_xpath", addleinandlevydata.get("Additional Payable Information1"));
			Thread.sleep(2000);
			clearValue("Startdate_xpath");
			input("Startdate_xpath", addleinandlevydata.get("Start Date1"));
			Thread.sleep(2000);
			input("Amount_xpath", addleinandlevydata.get("Amount1"));
			Thread.sleep(2000);
			click("SearchLienHolder_xpath");
			Thread.sleep(2000);
			input("InputInsideLien_xpath", addleinandlevydata.get("Lien Holder1") + Keys.TAB);
			Thread.sleep(2000);
			click("SearchInsideLien_xpath");
			Thread.sleep(2000);
			click("OkLienandLevy_xpath");
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Apply_xpath");
			Thread.sleep(3000);
			// addPaymentDeductionLienandLevies(testname);
			System.out.println("addLienandLevies method is finshed");
			//addPaymentDeduction(testName);
			logger.log(LogStatus.INFO, "Successfully added Lien and Levies");
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Facing some issue in adding Lien and Levies", testName, nameofCurrMethod);
		}
	}

	public void addPaymentDeductionLienandLevies(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
		switchToFrame("tapestry");
		click("Update_xpath");
		popuphandler();
		switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");
		click("AddPaymentDeductions_xpath");

		HashMap<String, String> paymentdeductionlienandleviesdata = getData(Constants.INPUT_XLS,
				"Payment Deduction Lien Levy", testName);
		Thread.sleep(5000);

		getElement("StartDatePaymentDeductions_xpath").sendKeys(Keys.HOME);
		input("StartDatePaymentDeductions_xpath", paymentdeductionlienandleviesdata.get("Start Date1"));
		Thread.sleep(2000);
		getElement("EndDatePaymentDeductions_xpath").sendKeys(Keys.HOME);
		input("EndDatePaymentDeductions_xpath", paymentdeductionlienandleviesdata.get("Stop Date1"));
		Thread.sleep(2000);

		click("DeductionTypePaymentDeduction_xpath");
		selectDDByVisibleText("DeductionTypePaymentDeduction_xpath",
				paymentdeductionlienandleviesdata.get("Deduction Type1"));
		if (paymentdeductionlienandleviesdata.get("Use Table2").equalsIgnoreCase("OFF")) {
			click("UseTablePaymentDeduction_xpath");
			Thread.sleep(2000);
		}

		// selectDDByVisibleText("FilingStatus_xpath",
		// paymentdeductionlienandleviesdata.get("Filing Status2"));
		// input("Exemptions_xpath",
		// paymentdeductionlienandleviesdata.get("Exemptions2"));
		input("AdditionalAmount_xpath", paymentdeductionlienandleviesdata.get("Additional Amount1"));
		Thread.sleep(2000);
		input("Priority_xpath", paymentdeductionlienandleviesdata.get("Priority1"));
		Thread.sleep(2000);
		click("EditPayment_xpath");
		Thread.sleep(2000);
		popuphandler();
		switchToDiv("InputPayment_xpath");
		input("InputPayment_xpath", paymentdeductionlienandleviesdata.get("Lien Holder1") + Keys.TAB);
		Thread.sleep(2000);
		click("SearchPayment_xpath");
		Thread.sleep(2000);
		popuphandler();
		switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");
		selectDDByVisibleText("ThirdPartyPaymentDeduction_xpath",
				paymentdeductionlienandleviesdata.get("Th. Party Payment1"));
		Thread.sleep(2000);
		click("CheckboxPaymentStreams_xpath");
		Thread.sleep(2000);
		click("ApplyPaymentStreams_xpath");
		Thread.sleep(2000);
		click("SavePayment_xpath");
		Thread.sleep(2000);
		click("closePaymentStreams_xpath");
		System.out.println("addPaymentDeductionLienandLevies method is finshed");
		//addPaymentDeduction(testName);
		logger.log(LogStatus.INFO, "Payment deduction added successfully");
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Some issue in adding  Payment Deduction Lien and Levies", testName, nameofCurrMethod);
		}
	}
	
	/*public void addPaymentDeduction(String testName) throws Exception {

		try {
		switchToFrame("tapestry");
		click("Update_xpath");
		popuphandler();
		switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");
		click("AddPaymentDeductions_xpath");

		HashMap<String, String> paymentdeductiondata = getData(Constants.INPUT_XLS, "Ann Payment Deduction", testName);	// Added Ann in Sheet name in Oct
		Thread.sleep(5000);

		click("StartDatePaymentDeductions_xpath");
		input("StartDatePaymentDeductions_xpath", paymentdeductiondata.get("Start Date2"));
		Thread.sleep(2000);

		click("DeductionTypePaymentDeduction_xpath");
		selectDDByVisibleText("DeductionTypePaymentDeduction_xpath", paymentdeductiondata.get("Deduction Type2"));
		if (paymentdeductiondata.get("Use Table2").equalsIgnoreCase("OFF")) {
			click("UseTablePaymentDeduction_xpath");
			Thread.sleep(2000);
		}

		selectDDByVisibleText("FilingStatus_xpath", paymentdeductiondata.get("Filing Status2"));
		input("Exemptions_xpath", paymentdeductiondata.get("Exemptions2"));
		input("AdditionalAmount_xpath", paymentdeductiondata.get("Additional Amount2"));
		input("PaymentDeductionPercent_xpath", paymentdeductiondata.get("Percent2"));
		click("CheckboxPaymentStreams_xpath");
		Thread.sleep(2000);
		click("ApplyPaymentStreams_xpath");
		Thread.sleep(2000);
		click("SavePayment_xpath");
		Thread.sleep(2000);
		click("closePaymentStreams_xpath");
		System.out.println("addPaymentDeduction method is finshed");
		addPaymentmethod(testName);
		logger.log(LogStatus.INFO, "Successfully added Payment Deduction");
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "some issue in adding Payment Deduction", testName);
		}
	}*/
	
	public void addPaymentDeduction(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("benefitsLink_xpath");
			Thread.sleep(2000);
			click("Benefits_xpath");
			Thread.sleep(2000);
			explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
			switchToFrame("tapestry");
			click("PaymentSetupTab_xpath");
			Thread.sleep(2000);
			click("ExpandPD_xpath");
			scrollWindow("CogwheelPaymentDeduction_xpath");
			click("CogwheelPaymentDeduction_xpath");
			click("EditDetailsPD_xpath");
			Thread.sleep(2000);
			popuphandler();
			// switchToDiv("SwitchEditDetails_xpath");
			switchToFrame("DisbursementPages_DeductionTemplatePopupPage");
			click("AddbuttonPD_xpath");
			HashMap<String, String> paymentdeductiondata = getData(Constants.INPUT_XLS, "Ann Payment Deduction",testName);
			Thread.sleep(2000);
			selectDDByVisibleText("DeductionTypePaymentDeduction_xpath", paymentdeductiondata.get("Deduction Type2"));
			Thread.sleep(2000);
			selectDDByVisibleText("FilingStatus_xpath", paymentdeductiondata.get("Filing Status2"));
			Thread.sleep(2000);
			input("Exemptions_xpath", paymentdeductiondata.get("Exemptions2"));
			Thread.sleep(2000);
			click("OkButton_xpath");
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
		}

		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "some issue in adding Payment Deduction", testName, nameofCurrMethod);
		}
	}

	
	public void addPaymentmethod(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
		switchToFrame("tapestry");
		HashMap<String, String> paymentmethoddata = getData(Constants.INPUT_XLS, "Payment Method", testName);
		Thread.sleep(2000);
		click("Update_xpath");
		popuphandler();
		switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");
		click("AddPeriod_xpath");
		Thread.sleep(2000);
		input("StarDatePaymentMethod_xpath", paymentmethoddata.get("Start Date"));
		click("OkPaymentMethod_xpath");
		Thread.sleep(2000);
		selectDDByVisibleText("PmtMode_xpath", paymentmethoddata.get("PM1_Pmt Mode"));
		Thread.sleep(2000);
		input("BankName_xpath", paymentmethoddata.get("PM1_Bank Name"));
		Thread.sleep(2000);
		input("RoutingNo_xpath", paymentmethoddata.get("PM1_Routing No"));
		Thread.sleep(2000);
		selectDDByVisibleText("AccountType_xpath", paymentmethoddata.get("PM1_Recipient Account Type"));
		Thread.sleep(2000);
		input("AccountNumber_xpath", paymentmethoddata.get("PM1_Recipient Account No"));
		Thread.sleep(2000);
		selectDDByVisibleText("PaymentMethodStatus_xpath", paymentmethoddata.get("PM1_Status"));
		Thread.sleep(2000);
		input("PercentageSplit_xpath", paymentmethoddata.get("PM1_% to Split"));
		Thread.sleep(2000);
		click("CheckboxPaymentStreams_xpath");
		Thread.sleep(2000);
		click("ApplyPaymentStreams_xpath");
		Thread.sleep(2000);
		click("SavePayment_xpath");
		Thread.sleep(2000);
		click("closePaymentStreams_xpath");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Save_xpath");
		Thread.sleep(2000);
		logger.log(LogStatus.INFO, "Payment method added successfully");
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Some issue in add payment method", testName, nameofCurrMethod);
		}
	}
	
public void AddPaymentmethod(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			click("Benefits_xpath");
			switchToFrame("tapestry");
			Thread.sleep(2000);
			click("PaymentSetupTab_xpath");
			Thread.sleep(2000);
			click("ActionGear_xpath");
			click("ModifyStream_xpath");
			Thread.sleep(1000); 
			enterKey();
			Thread.sleep(2000);
		HashMap<String, String> paymentmethoddata = getData(Constants.INPUT_XLS, "Payment Method", testName);
		Thread.sleep(2000);
		click("ExpandPaymentMethod_xpath");
		Thread.sleep(1000);
		scrollWindow("ScheduledAdjustment_xpath");
		Thread.sleep(2000);
		click("EditPaymentMehtod_xpath");
		Thread.sleep(2000);
		click("EditDetailsPaymentMethod_xpath");
		Thread.sleep(2000);
		switchToFrame("DisbursementPages_EFTAuthorizationPopupPage");
		click("AddPeriod_xpath");
		Thread.sleep(1000);
		input("StarDatePaymentMethod_xpath", paymentmethoddata.get("Start Date"));
		click("OkPaymentMethod_xpath");
		Thread.sleep(2000);
		selectDDByVisibleText("PmtMode_xpath", paymentmethoddata.get("PM1_Pmt Mode"));
		Thread.sleep(2000);
		input("BankName_xpath", paymentmethoddata.get("PM1_Bank Name"));
		Thread.sleep(2000);
		input("RoutingNo_xpath", paymentmethoddata.get("PM1_Routing No"));
		Thread.sleep(2000);
		selectDDByVisibleText("AccountType_xpath", paymentmethoddata.get("PM1_Recipient Account Type"));
		Thread.sleep(2000);
		input("AccountNumber_xpath", paymentmethoddata.get("PM1_Recipient Account No"));
		Thread.sleep(2000);
		selectDDByVisibleText("PaymentMethodStatus_xpath", paymentmethoddata.get("PM1_Status"));
		Thread.sleep(2000);
		input("PercentageSplit_xpath", paymentmethoddata.get("PM1_% to Split"));
		Thread.sleep(2000);
		click("OkPaymentMethodEditDetails_xpath");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Save_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		switchToDiv("ConfirmMessagesOk_xpath");
		logger.log(LogStatus.INFO, "Payment method added successfully");
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Some issue in add payment method", testName, nameofCurrMethod);
		}
	}
	
	
	
public void updatePaymentmethod(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Benefits_xpath");
		switchToFrame("tapestry");
		Thread.sleep(2000);
		click("PaymentSetupTab_xpath");
		Thread.sleep(2000);
		click("ActionGear_xpath");
		click("ModifyStream_xpath");
		Thread.sleep(1000); 
		enterKey();
		Thread.sleep(2000);
		HashMap<String, String> paymentmethoddata = getData(Constants.INPUT_XLS, "Update_Payment Method", testName);
		click("ExpandPaymentMethod_xpath");
		Thread.sleep(1000);
		scrollWindow("ScheduledAdjustment_xpath");
		Thread.sleep(2000);
		click("EditPaymentMehtod_xpath");
		Thread.sleep(2000);
		click("EditDetailsPaymentMethod_xpath");
		Thread.sleep(2000);
		switchToFrame("DisbursementPages_EFTAuthorizationPopupPage");
		/*click("Update_xpath");
		popuphandler();
		switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");*/
		click("AddPeriod_xpath");
		Thread.sleep(1000);
		input("StarDatePaymentMethod_xpath", paymentmethoddata.get("Start Date"));
		click("OkPaymentMethod_xpath");
		Thread.sleep(2000);
		selectDDByVisibleText("PmtMode_xpath", paymentmethoddata.get("PM1_Pmt Mode"));
		Thread.sleep(2000);
		input("BankName_xpath", paymentmethoddata.get("PM1_Bank Name"));
		Thread.sleep(2000);
		input("RoutingNo_xpath", paymentmethoddata.get("PM1_Routing No"));
		Thread.sleep(2000);
		selectDDByVisibleText("AccountType_xpath", paymentmethoddata.get("PM1_Recipient Account Type"));
		Thread.sleep(2000);
		input("AccountNumber_xpath", paymentmethoddata.get("PM1_Recipient Account No"));
		Thread.sleep(2000);
		selectDDByVisibleText("PaymentMethodStatus_xpath", paymentmethoddata.get("PM1_Status"));
		Thread.sleep(2000);
		input("PercentageSplit_xpath", paymentmethoddata.get("PM1_% to Split"));
		Thread.sleep(2000);
		click("OkPaymentMethodEditDetails_xpath");
		/*click("CheckboxPaymentStreams_xpath");
		Thread.sleep(2000);
		click("ApplyPaymentStreams_xpath");
		Thread.sleep(2000);
		click("SavePayment_xpath");
		Thread.sleep(2000);
		click("closePaymentStreams_xpath");*/
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Save_xpath");
		Thread.sleep(2000);
		logger.log(LogStatus.INFO, "Payment method added successfully");
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Some issue in add payment method", testName, nameofCurrMethod);
		}
	}
	

public void deletePaymentmethod(String testName) throws Exception {
	
	String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

	try {
	searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
	click("Benefits_xpath");
	switchToFrame("tapestry");
	Thread.sleep(2000);
	click("PaymentSetupTab_xpath");
	Thread.sleep(2000);
	click("ActionGear_xpath");
	click("ModifyStream_xpath");
	Thread.sleep(1000);
	enterKey();
	Thread.sleep(2000);
	click("ExpandPaymentMethod_xpath");
	Thread.sleep(1000);
	scrollWindow("ScheduledAdjustment_xpath");
	Thread.sleep(2000);
	click("EditPaymentMehtod_xpath");
	Thread.sleep(2000);
	click("EditDetailsPaymentMethod_xpath");
	Thread.sleep(2000);
	switchToFrame("DisbursementPages_EFTAuthorizationPopupPage");
	/*click("Update_xpath");
	popuphandler();
	switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");*/
	click("BankName_xpath");
	Thread.sleep(2000);
	click("DeletePayment_xpath");
	Thread.sleep(1000);
	acceptAlert();
	Thread.sleep(1000);
	click("OkPaymentMethodEditDetails_xpath");
	Thread.sleep(1000);
	driver.switchTo().defaultContent();
	click("Save_xpath");
	Thread.sleep(2000);
	switchToFrame("tapestry");
	switchToDiv("ConfirmMessagesOk_xpath");
	logger.log(LogStatus.INFO, "Payment method added successfully");
}
	catch (Exception e) {
		e.printStackTrace();
		reportingFail(logger, "Some issue in add payment method", testName, nameofCurrMethod);
	}
}
public void QDROSplit(String testname) throws Exception {
		
		//add QDRO in relationship and DRO details
		
		System.out.println("entered into payment stream");
		click("CollapseExpand_xpath");
		String status= getElement("Status_xpath").getAttribute("value");
		if(status.equalsIgnoreCase("Pending"))
		{
			Thread.sleep(2000);
		
			/*click("ActionGear_xpath");
			click("ModifyStream_xpath");
			Thread.sleep(1000); 
			enterKey();*/
			Thread.sleep(4000); 
			click("FactoringSplit_xpath");
			Thread.sleep(2000); 
			popuphandler();	
			Thread.sleep(2000); 
			click("PayeeSplitGear_xpath");
			Thread.sleep(2000); 


			click("Split_xpath");
			Thread.sleep(2000);
			enterKey();
			//click("OkAlert_xpath");
			Thread.sleep(2000);
			click("OK_xpath");
			/*enterKey();
			Thread.sleep(2000); 	
			enterKey();
			Thread.sleep(2000); 	
			enterKey();
			Thread.sleep(2000); 	
			enterKey();*/
			driver.switchTo().parentFrame();
			
			click("Apply_xpath");
			
			Thread.sleep(2000); 
			click("Save_xpath");
		}  
			
			/*Thread.sleep(2000); 
			loginWithDifferentUserID(testname);
		
			GotoPaymentSetup();
	
			if(status.equalsIgnoreCase("Approval Pending"))
			{
			click("CollapseExpand_xpath");
			click("StreamChange_xpath");
			driver.switchTo().defaultContent();
			switchToFrame("DisbursementPages_PaymentStreamChangesPopup");
			Thread.sleep(2000); 
			
			click("review_xpath");
			Thread.sleep(2000); 
			
		
			click("Approve_xpath");
			driver.switchTo().defaultContent();
			System.out.println("QDRO application created in related payment stream");
			}*/
		
	}
	
	public void QDROCaseactivation(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
	
		try {
			
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		Thread.sleep(2000);
		System.out.println("entered into QDRO case activation");
		GotoPaymentSetup();
		switchToFrame("tapestry");
		Thread.sleep(2000);
		click("CollapseExpand_xpath");
		//String status= getElement("Status_xpath").getAttribute("value");
		//if(status.equalsIgnoreCase("In Pay"))
		//{
			scrollWindow("GotoApp_xpath");	//---------------xpath changed
			Thread.sleep(2000);
			click("GotoApp_xpath");
			Thread.sleep(3000);
			driver.switchTo().parentFrame();
			click("Edit_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
		
			click("CollapseExpand_xpath");
			click("Update_xpath");
			driver.switchTo().defaultContent();
			switchToFrame("DisbursementPages_MassEFTdeductionsUpdatePopupPage");
			Thread.sleep(2000);
			addPaymentDeduction(testName);			
			Thread.sleep(2000);
			//click("Save_xpath");//---------------error
			driver.switchTo().defaultContent();
			switchToFrame("tapestry");
			Thread.sleep(2000);
			click("PFGear_xpath");
			Thread.sleep(2000);
			click("BankDeduct_xpath");
			Thread.sleep(2000);
			click("PFGear_xpath");
			Thread.sleep(2000);
			click("SetupComplete_xpath");
			//approve from other id--------PF steps-----------eneter-----update--------
			//status -inpay
		
		}
		
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Some issue in add payment method", testName, nameofCurrMethod);
		}	
	}	
	
	public void GotoPaymentSetup() throws Exception
	{
		/*driver.switchTo().defaultContent();
		click("Cancel_xpath");
		Thread.sleep(2000);*/
	    switchToDiv("benefitsLink_xpath");
		Thread.sleep(2000);
		click("benefitsLink_xpath");
		Thread.sleep(2000); 
		switchToFrame("tapestry");
		click("PaymentSetup_xpath");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Cancel_xpath");
		Thread.sleep(2000);
		click("confirmcancelyes_xpath");
		Thread.sleep(2000);
		
	}

}
