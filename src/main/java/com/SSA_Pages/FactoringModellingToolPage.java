package main.java.com.SSA_Pages;

import java.awt.AWTException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.GenericClass.XLSReader;

public class FactoringModellingToolPage extends GenericClass {
	
	XLSReader xl = new XLSReader(Constants.OUTPUT_XLS);
	int factoringCompanyListLength;
	String factoringCompanySheetStatus;
	
	public void FactoringModelingUIOrganisationTableData(String testName, String factoringCompanySheetStatus)
			throws Exception {

		HashMap<String, String> aceBenefitSegmentsData = fetchBnefitSegmentsDetails(testName,
				factoringCompanySheetStatus);

		driver.switchTo().defaultContent();
		explicitwait("main_menu_xpath", 20, "visibilityOfElementLocated");
		click("main_menu_xpath");
		Thread.sleep(2000);
		click("Links_xpath");

		Thread.sleep(2000);
		click("Links-FactoringModelingTool_xpath");
		// Navigate on Factoring modeling tool

		popuphandler("FactoringModelingUI");

		click("GenerateNew-button_xpath");
		// finding Number of page on factoring ModelUI
		explicitwait("TotalNumberOfPages_xpath", 20, "visibilityOfElementLocated");
		String totalPageString = getElement("TotalNumberOfPages_xpath").getAttribute("innerText");

		// Converting Number of page into int value
		totalPageString = totalPageString.substring(totalPageString.indexOf(" ") + 1);
		int totalPageNumber = Integer.parseInt(totalPageString);

		// finding Organisation-Contained Table
		while (totalPageNumber > 0) {

			// finding number of table in single page
			int numberOfTableInPage = getElements("NumberOf-Table-InPage_xpath").size();
			System.out.println("Number of tables in current page = " + numberOfTableInPage);

			// Loop for iterating tables
			for (int tableNumber = 1; tableNumber <= numberOfTableInPage; tableNumber++) {

				String paymentAmount = driver
						.findElement(By.xpath("(//div[@class='paymentStream'])[" + tableNumber + "]//tbody//tr/td[2]"))
						.getText();
				String foaName = driver
						.findElement(By.xpath("(//div[@class='paymentStream'])[" + tableNumber + "]//tbody//tr/td[3]"))
						.getText();

				if (aceBenefitSegmentsData.get("AnnuityOptionName" + tableNumber).equals(foaName)
						&& aceBenefitSegmentsData.get("BasicAmount" + tableNumber).equals(paymentAmount)) {
					// ---------Dhanraj

					int numberOfColInTable = driver.findElements(By.xpath("//div[@class='paymentStream' and position()="
							+ tableNumber + "]//table[contains(@class, 'table-striped')]//th")).size();
					for (int fieldHeading = 1; fieldHeading <= numberOfColInTable; fieldHeading++) {

						// String replaceValue3 = Integer.toString(fieldHeading);

						String fieldHeadingName = driver
								.findElement(By.xpath("//div[@class='paymentStream' and position()=" + tableNumber
										+ "]//table[contains(@class, 'table-striped')]//th" + "[" + fieldHeading + "]"))
								.getAttribute("innerText");

						if (fieldHeadingName.equalsIgnoreCase("Payment Amount")) {

							for (int rows = 1; rows <= 6; rows++) {

								// Getting row first field
								String firstColFieldValue = firstColFieldValueMethod(tableNumber, rows);

								// Getting dynamic column
								String colFieldValue = colFieldValueMethod(tableNumber, rows, fieldHeading);

								xl.setCellDataValue(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus),
										firstColFieldValue, "Payment Amount", colFieldValue, 2);

							}
						}

						else if (fieldHeadingName.equalsIgnoreCase("FOA")) {

							for (int rows = 1; rows <= 6; rows++) {

								// Getting dynamic column
								String colFieldValue = colFieldValueMethod(tableNumber, rows, fieldHeading);

								// Getting row first field
								String firstColFieldValue = firstColFieldValueMethod(tableNumber, rows);

								xl.setCellDataValue(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus),
										firstColFieldValue, "FOA", colFieldValue, 2);
							}
						}

						else if (fieldHeadingName.equalsIgnoreCase("Increase")) {

							for (int rows = 1; rows <= 6; rows++) {

								// Getting dynamic column
								String colFieldValue = colFieldValueMethod(tableNumber, rows, fieldHeading);

								// Getting row first field
								String firstColFieldValue = firstColFieldValueMethod(tableNumber, rows);

								xl.setCellDataValue(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus),
										firstColFieldValue, "Increase", colFieldValue, 2);
							}
						}

						// condtion for matching company name
						for (int factoringCompany = 1; factoringCompany <= factoringCompanyListLength; factoringCompany++) {

							System.out.println("Name of factoring company ="
									+ aceBenefitSegmentsData.get("FactoringCompany" + factoringCompany));

							if (fieldHeadingName.equalsIgnoreCase(
									aceBenefitSegmentsData.get("FactoringCompany" + factoringCompany))) {

								for (int rows = 1; rows <= 6; rows++) {

									// Getting dynamic column
									String colFieldValue = colFieldValueMethod(tableNumber, rows, fieldHeading);

									// Getting row first field
									String firstColFieldValue = firstColFieldValueMethod(tableNumber, rows);

									xl.setCellDataValue(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus),
											firstColFieldValue,
											aceBenefitSegmentsData.get("FactoringCompany" + factoringCompany),
											colFieldValue, 2);
								}
							}
						}
					}
					// This method will fetch all the projection values from factoring modeling tool
					viewPaymentProjections(factoringCompanySheetStatus);
				}

			}
			totalPageNumber--;
		}

		// Method use to Open micro and click on it for result value
		// XLSReader xls = new XLSReader(Constants.MICRO_XLS);

	}
	
	
	
	// method use to extract data of different Foa's from ace application
	public HashMap<String, String> fetchBnefitSegmentsDetails(String testName, String factoringCompanySheetStatus) throws IOException, InterruptedException {
		
		System.out.println("factoringCompanySheetStatus : " + factoringCompanySheetStatus);
		HashMap<String, String> data=getData(Constants.INPUT_XLS,"Annuitant",testName);
		HashMap<String, String> aceData = new HashMap<String, String>();

		/*input("searchBox_xpath", data.get("SSN"));
		click("searchButton_xpath");
		Thread.sleep(2000);*/
		//click("AnnutantSSN_xpath");
		/* driver.switchTo().defaultContent();
		 Thread.sleep(2000);
		 click("benefitsLink_xpath");*/
		//Fetching values from ACE
		
		//Code added on 01/15/2020
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		Thread.sleep(2000);
		switchToDiv("benefitsLink_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		
	//	switchToFrame("tapestry");
		int noOfFoas=driver.findElements(By.xpath("//table[@id='BENEFIT_SEGMENTS_GRID']//tr[@name='itemdetails']")).size();
		System.out.println("Number of foa's:"+noOfFoas);
		for(int i=1;i<=noOfFoas;i++) {
		
			String annuityOptionName =driver.findElement(By.xpath("(//table[@id='BENEFIT_SEGMENTS_GRID']//tr[@name='itemdetails'])["+i+"]/td[5]/span")).getText();
			annuityOptionName = foaShortName(annuityOptionName);
			String basicAmount=driver.findElement(By.xpath("(//table[@id='BENEFIT_SEGMENTS_GRID']//tr[@name='itemdetails'])["+i+"]/td[6]/span")).getText();
			aceData.put("AnnuityOptionName"+i, annuityOptionName);
			aceData.put("BasicAmount"+i, basicAmount);
		}
			// Adding code for factoring company from Ace application ------------------------------------ Dhanraj -=--------------------------
			click("Factoring_xpath");
			factoringCompanyListLength = getElements("NumberOfFactoringCompany_xpath").size();
			
			System.out.println("factoringCompanyListLength ="+factoringCompanyListLength);
						
			for(int factoringCompany = 1; factoringCompany <= factoringCompanyListLength; factoringCompany++) {
				String factoringCompanyString = Integer.toString(factoringCompany); 
				
				String factoringCompanyName = getElementWithReplacementValue(replacementInLocator("FactoringcompanyName_xpath",factoringCompanyString), "xpath").getText();
				String factoringCompanyWF= getElementWithReplacementValue(replacementInLocator("FactoringcompanyWF_xpath",factoringCompanyString), "xpath").getText();
				String factoringCompanyConcatedName = factoringCompanyName+" / WF"+factoringCompanyWF;
				System.out.println("factoringCompanyName = "+factoringCompanyConcatedName);
				xl.setCellDataThroghStaticRow$Col(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus), 2, (6+factoringCompany), factoringCompanyConcatedName);
				xl.setCellDataThroghStaticRow$Col(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus), 2, (18+factoringCompany), factoringCompanyConcatedName);
				xl.setCellDataThroghStaticRow$Col(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus), 10, (4+factoringCompany), factoringCompanyConcatedName);
				xl.setCellDataThroghStaticRow$Col(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus), 10, (16+factoringCompany), factoringCompanyConcatedName);
				aceData.put("FactoringCompany"+factoringCompany, factoringCompanyConcatedName);
			}
		return aceData;
	}
	
	

	public String colFieldValueMethod(int tableNumber, int rows, int fieldHeading) {
		
		String colFieldValue = getElementWithReplacementValue("//div[@class='paymentStream' and position()="+tableNumber+"]//table[contains(@class, 'table-striped')]//tr["+rows+"]/td["+fieldHeading+"]", "xpath").getAttribute("innerText");
		return colFieldValue;
	}
	
	public String firstColFieldValueMethod(int tableNumber, int rows) {
		
		String firstColFieldValue = getElementWithReplacementValue("//div[@class='paymentStream' and position()="+tableNumber+"]//table[contains(@class, 'table-striped')]//tr["+rows+"]/td[1]", "xpath").getAttribute("innerText");
		return firstColFieldValue;
	}
			
	// ---- 5-1-2019(Wednesday)
	public void viewPaymentProjections(String factoringCompanySheetStatus) throws IOException, InterruptedException {
		
		
		click("ViewPaymentProjections(s)_xpath");
		Thread.sleep(2000);
		
		List<WebElement> viewPaymentProjectionsTableHeader = getElements("ListOfHeaderIn-ViewPaymentProject(s)_xpath");
		int numberOfHeaderInviewPaymentProjectionsTable = viewPaymentProjectionsTableHeader.size();
		System.out.println(numberOfHeaderInviewPaymentProjectionsTable);
		
		int listOfSubTableInViewPaymentProjectionsTable = getElements("ListOfSubTableIN-ViewPaymentProject(s)_xpath").size();
		
		// Loop is use to iterate sub Tables of ViewPaymentProjections(s)
		for(int subTable = 1; subTable<= listOfSubTableInViewPaymentProjectionsTable; subTable++) {
			String subTableString = Integer.toString(subTable); 
			clickDynamicElement(replacementInLocator("PlusSymbolPaymentDate_xpath", subTableString), "xpath");
			Thread.sleep(2000);
			
			List<WebElement> listOfDataRowsInSubTable = getElementsWithReplacementValue(replacementInLocator("ListOfRowsIn-SubTable_xpath", subTableString));
			int numberOfDataRowsInSubTable = listOfDataRowsInSubTable.size();
			System.out.println(numberOfDataRowsInSubTable);
			
			// iterate according header
			for(int header = 1; header <= numberOfHeaderInviewPaymentProjectionsTable; header++) {
				 
				//iterate for total number of rows in sub tables
				for(int row = 2 ; row <= numberOfDataRowsInSubTable; row++) {
					
					String particularCellData = driver.findElement(By.xpath("//table[@class='table paymentsTable']/tbody/payment-subtable["+subTable+"]//tr["+row+"]/td["+header+"]")).getText();
					String firstColFieldValue = driver.findElement(By.xpath("//table[@class='table paymentsTable']/tbody/payment-subtable["+subTable+"]//tr["+row+"]/td[1]")).getText();
					
					System.out.println("particularCellData = "+ particularCellData+"   firstColFieldValue = "+firstColFieldValue);
					
					xl.setCellDataValue(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus),firstColFieldValue, (viewPaymentProjectionsTableHeader.get(header-1)).getText(), particularCellData, 10);
				}
			}
		}
	}
	
	// Method give you appropriate excel sheet name according factoring company status
	public String xcellSheetNameForFactoringCompany(String status) {

		String sheetName = "";

		switch (status) {
		case "Open":
			sheetName = "Open";
			break;
		case "Court Order Received":
			sheetName = "Court Order Received";
			break;
		case "In Production":
			sheetName = "In Production";
			break;
		case "":
			sheetName = "Without Factoring";
			break;
		default:
			System.out.println("case not matched");
		}
		return sheetName;
	}
	
	/*public void xcellCompareMethod() throws IOException, AWTException, InterruptedException {
		xl.xcellCompareMethod(xcellSheetNameForFactoringCompany(factoringCompanySheetStatus));
	}*/
	
}

