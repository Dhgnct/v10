package main.java.com.SSA_Pages;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.login.Login;

public class Quote_Revision extends GenericClass {
	public static Logger log = Logger.getLogger(Quote_Revision.class.getName());

	public void createQuoteRevision(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		
		try {
						
			HashMap<String, String> QuoteRevise = getData(Constants.INPUT_XLS, "Benefit_Details", testName);
			 /*String eftnoValue = "";
			 String overPaymentStatusValue = "";
			 String eftnoValueCheck = "";
			 boolean flag = false;*/

			clearValue("Search_xpath");
			search("Search_xpath", annuitantAltId("Annuitant", testName));
			Thread.sleep(2000);
			String mainWindow = driver.getWindowHandle();
			switchToDiv("Edit_xpath");
			switchToDiv("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			// Thread.sleep(3000);
			getElement("Benefits_Cogwheel_xpath").click();
			getElement("Benefits_CogwheelOption_xpath").click();
			popuphandler("Update Purchase Price");

			// Enter Value in Revised Purchased Amount in Text Box.

			getElement("Benefits_RevisedPurchasedAmount_xpath").clear();
			Thread.sleep(2000);

			acceptAlert();
			Thread.sleep(2000);

			/*String qouteRev=QuoteRevise.get("Revised Purchase Amount");
			
			getElement("Benefits_RevisedPurchasedAmount_xpath").sendKeys(qouteRev);*/
			
			getElement("Benefits_RevisedPurchasedAmount_xpath").sendKeys("300000");
			
			click("Benefits_OkButton_xpath");
			
			//click("Benefits_CloseButton_xpath");
			
			String benefitAdjustment = getElement("Benefits_Adjustment_xpath").getAttribute("value");
			int benValue = Integer.parseInt(benefitAdjustment.replaceAll("[^0-9]", ""));
			String benefitValue = String.valueOf(benValue);
			int counted = benefitValue.length();
			System.out.println(counted);
			String benefitValueUpdated = benefitValue.substring(0, counted - 2);
			
			click("Benefits_YesButton_xpath");
			// click("Benefits_CloseButton_xpath");
			Thread.sleep(8000);
			
			
			driver.switchTo().window(mainWindow);
						
			click("applyBtn_xpath");
			click("ContractsAndCerts_xpath");
			switchToFrame("tapestry");
			// acceptAlert();
			click("ContractsAndCerts_Transactions_xpath");

			selectDDByVisibleText("Transactions_TransStatus_xpath", "All");

			click("Transactions_FilterButton_xpath");
			Thread.sleep(3000);

			int count = getElements("Transactions_TableList_xpath").size();

			System.out.println("Count=" + count);

			for (int trans = 1; trans < count; trans++) {
				String replaceValue = String.valueOf(trans);

				String updatedStatusLocator = replacementInLocator("Transactions_StatusCol_xpath", replaceValue);
				String transStatusValue = getElementWithReplacementValue(updatedStatusLocator, "xpath").getText();

				System.out.println("transstatusValue= " + transStatusValue);
				if (transStatusValue.equalsIgnoreCase("Open")) {
					click("Transactions_Cogwheel_xpath");
					// Thread.sleep(1000);

					click("Transactions_CogwheelValue_xpath");

					switchToFrame("WorldPages_PaymentPage");

					click("Payments_GridControls_xpath");
					Thread.sleep(1000);

					click("Payments_AddButton_xpath");

					// String CheckValue = Transactions.get("Check Value");

					getElement("Payments_CheckNumber_xpath").sendKeys("123445");

					selectDDByValue("Payments_PaymentType_xpath", "CH");

					String Bal = getElement("Payments_Balance_xpath").getAttribute("value");
					String Balance = Bal.substring(1);

					getElement("Payments_PaymentAmount_xpath").sendKeys(Balance);

					getElement("Payments_DepositClearedDate_xpath").sendKeys(Keys.HOME);

					getElement("Payments_DepositClearedDate_xpath").sendKeys("01/01/2019");

					click("Payments_SaveButton_xpath");

					click("Payments_EditButton_xpath");
					Thread.sleep(1000);

					click("Payments_SelectToApply_xpath");
					
					//Added Loc
					click("Payments_TransChkbox_xpath");

					click("Payments_SaveButton_xpath");

					click("Payments_CloseButton_xpath");
					Thread.sleep(3000);

					break;
				}
			}
			switchToFrame("tapestry");
			//Comment Updated as Quote Revision scenario is changed. 
			/*for (int transupdate = 1; transupdate < count; transupdate++) {
				String replaceVal = String.valueOf(transupdate);

				String CommStatus = replacementInLocator("Transactions_CommissionStatusValue_xpath", replaceVal);
				String CommStatusValue = getElementWithReplacementValue(CommStatus, "xpath").getText();
				System.out.println("Commission Status Value=" + CommStatusValue);
				if (CommStatusValue.equalsIgnoreCase("Adjustment Pending")) {
					System.out.println("Adjustment Pending value found");
				}
			}*/
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(5000);

			loginWithDifferentUserID("CI_TC_10");
			clearValue("Search_xpath");
			search("Search_xpath", annuitantAltId("Annuitant", testName));
			Thread.sleep(2000);
			switchToDiv("Edit_xpath");
			click("ContractsAndCerts_xpath");
			switchToFrame("tapestry");
			// acceptAlert();
			
			
			
			click("ContractsAndCerts_Transactions_xpath");

			selectDDByVisibleText("Transactions_TransStatus_xpath", "All");

			click("Transactions_FilterButton_xpath");
			Thread.sleep(3000);

			switchToFrame("tapestry");
			int countelly = getElements("Transactions_TableList_xpath").size();
			for (int transApprove = 2; transApprove < countelly; transApprove++) {
				String replaceVal = String.valueOf(transApprove);

				String CommStatus = replacementInLocator("Transactions_CommissionStatusValue_xpath", replaceVal);
				String CommStatusValue = getElementWithReplacementValue(CommStatus, "xpath").getText();
				System.out.println("Commission Status Value=" + CommStatusValue);
				if (CommStatusValue.equalsIgnoreCase("Purchase Amount")) {

					System.out.println("Adjustment Pending value found");
				
				
					String CommCogwheel = replacementInLocator("Transactions_CommissionCogwheel_xpath", replaceVal);
					getElementWithReplacementValue(CommCogwheel, "xpath").click();

					String EditDetails = replacementInLocator("Transactions_Edit_xpath", replaceVal);
					getElementWithReplacementValue(EditDetails, "xpath").click();
					
					// On Edit Details of Transaction# popup			
					
					popuphandler();
					
					explicitwait("TransactionDetails_TableList_xpath", 5, "visibilityOfElementLocated");

					int details = getElements("TransactionDetails_TableList_xpath").size();
					System.out.println("Details:"+details);
					for (int transDetails = 1; transDetails < details; transDetails++) {
						String transRow = String.valueOf(transDetails);
						String status = replacementInLocator("TransactionDetails_StatusValue_xpath", transRow);
						String statusValue = getElementWithReplacementValue(status, "xpath").getText();

						if (statusValue.equalsIgnoreCase("Pending")) {

							System.out.println("Pending Status Found");
							String DetailsCogwheel = replacementInLocator("TransactionDetails_DetailsCogwheel_xpath",
									transRow);
							getElementWithReplacementValue(DetailsCogwheel, "xpath").click();
							
 							click("TransactionDetails_ApproveValue_xpath");
							Thread.sleep(5000);
							click("TransactionDetails_Save_xpath");
							Thread.sleep(2000);
							break;
						}
					}

					
					
					
					/*for (int transUpdate = 2; transUpdate < count; transUpdate++) {

						String replaceValue = String.valueOf(transUpdate);

						String updatedStatusLocator = replacementInLocator("Transactions_StatusCol_xpath",
								replaceValue);
						String transStatusValue = getElementWithReplacementValue(updatedStatusLocator, "xpath")
								.getText();
						System.out.println(transStatusValue);
						String UpdatedTypeLocator = replacementInLocator("TransactionsDetails_Type_xpath",
								replaceValue);
						String UpdatedTypeValue = getElementWithReplacementValue(UpdatedTypeLocator, "xpath").getText();
						System.out.println(UpdatedTypeValue);
												
						if (transStatusValue.equalsIgnoreCase("Closed")
								&& UpdatedTypeValue.equalsIgnoreCase("Assignment/Transfer Fee")) {
							fnLogout();
							Thread.sleep(5000);
							Login logobj = new Login();
							logobj.login("testName", "Processor_ID");
							Thread.sleep(2000);
							clearValue("Search_xpath");
							search("Search_xpath", annuitantAltId("Annuitant", testName));
							Thread.sleep(2000);
							switchToDiv("Edit_xpath");
							switchToDiv("Benefits_xpath");

							BenefitsPage benefitClass = new BenefitsPage();
							benefitClass.addBenefitSegment(testName);

						}
					}*/
					
					//driver.switchTo().window(mainWindow);
					
					click("Save_xpath");
					click("Benefits_xpath");
					Thread.sleep(2000);
					switchToFrame("tapestry");
					String headerText=getElement("Benefits_HeaderText_xpath").getText();
					System.out.println(headerText);
					if(headerText.contains("Setup In Progress")) {
					
						reportingPass(logger, "Test Case Executed Successfully", testName, nameofCurrMethod);
					
					break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Test Case Execution Unsuccessful", testName, nameofCurrMethod);
		}
	}
}
