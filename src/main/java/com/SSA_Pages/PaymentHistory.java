package main.java.com.SSA_Pages;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class PaymentHistory extends GenericClass {
	public static Logger log = Logger.getLogger(ContractsAndCerts.class.getName());

	public void createPaymentHistory(String testName) throws IOException {
		try {
			HashMap<String, String> paymentHistory = getData(Constants.INPUT_XLS, "Post Payment Processing", testName);
			String eftnoValue = "";

			clearValue("Search_xpath");
			search("Search_xpath", annuitantAltId("Annuitant", testName));
			Thread.sleep(2000);
			switchToDiv("Edit_xpath");
			Thread.sleep(2000);
			switchToDiv("Benefits_xpath");
			switchToFrame("tapestry");
			Thread.sleep(2000);
			click("PaymentHistory_xpath");
			int count = getElements("PaymentHistoryTableList_xpath").size();

			for (int payment = 1; payment < count; payment++) {
				String replaceValue = String.valueOf(payment);

				String updatedStatusLocator = replacementInLocator("PaymentHistoryStatusValue_xpath", replaceValue);
				String paymentStatusValue = getElementWithReplacementValue(updatedStatusLocator, "xpath").getText();

				/*
				 * if (paymentStatusValue.equalsIgnoreCase("Outstanding2")) {
				 * 
				 * String updatedCogwheelLocator =
				 * replacementInLocator("PaymentHistoryCogwheelValue_xpath", replaceValue);
				 * getElementWithReplacementValue(updatedCogwheelLocator, "xpath").click();
				 * 
				 * String optionAction = paymentHistory.get("Option Value 1");
				 * 
				 * if (optionAction.equalsIgnoreCase("Stop") ||
				 * optionAction.equalsIgnoreCase("Stop and Reissue") ||
				 * optionAction.equalsIgnoreCase("void") ||
				 * optionAction.equalsIgnoreCase("Void and Reissue")) {
				 * cogWheelOptions(optionAction); cogWheelOptionWindow(); } else {
				 * System.out.println("Options Out of Index!!"); } }
				 */

				// Updated on 2019/07/23 -Starts here

				if (paymentStatusValue.equalsIgnoreCase("Cashed")) {
					String eftno = replacementInLocator("PaymentHistory_eft_xpath", replaceValue);
					eftnoValue = getElementWithReplacementValue(eftno, "xpath").getText();

					String updatedCogwheelLocator = replacementInLocator("PaymentHistoryCogwheelValue_xpath",
							replaceValue);
					getElementWithReplacementValue(updatedCogwheelLocator, "xpath").click();

					String optionAction = paymentHistory.get("Option_Value_1");

					if (optionAction.equalsIgnoreCase("Stop") || optionAction.equalsIgnoreCase("void")
							|| optionAction.equalsIgnoreCase("Overpayment")) {

						cogWheelOptions(optionAction);
						cogWheelOptionWindow();
						switchToFrame("tapestry");
						for (int paymentRow = 1; paymentRow < count; paymentRow++) {

							String replaceValuePay = String.valueOf(paymentRow);
							String eftnoRow = replacementInLocator("PaymentHistory_eft_xpath", replaceValuePay);
							// System.out.println("Value of eft no Row:"+eftnoRow);
							String eftnoRowValue = getElementWithReplacementValue(eftnoRow, "xpath").getText();
							System.out.println("Check No found:" + eftnoRowValue);

							if (eftnoValue.equals(eftnoRowValue)) {

								String statusRow = replacementInLocator("PaymentHistory_status_xpath", replaceValuePay);
								String statusRowValue = getElementWithReplacementValue(statusRow, "xpath").getText();

								if (statusRowValue.equalsIgnoreCase("Stop Approval Pending")) {
									System.out.println("Status: " + statusRowValue + "And EFT No: " + eftnoRowValue);
									break;

								}
							}
						}

					} else {

						System.out.println("Options Out of Index!!");
					}
					break;
				}

				// Updated on 2019/07/23 -Ends here

			}

			// Below code is used for Approve EFT.
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(5000);
			loginWithDifferentUserID(testName);
			clearValue("Search_xpath");
			search("Search_xpath", annuitantAltId("Annuitant", testName));
			Thread.sleep(2000);
			switchToDiv("Edit_xpath");
			switchToDiv("Benefits_xpath");
			switchToFrame("tapestry");
			Thread.sleep(2000);
			click("PaymentHistory_xpath");
			Thread.sleep(2000);

			for (int paymentRow = 1; paymentRow < count; paymentRow++) {

				String replaceValuePay = String.valueOf(paymentRow);
				Thread.sleep(2000);

				String eftnum = replacementInLocator("PaymentHistory_eft_xpath", replaceValuePay);
				String eftnumValue = getElementWithReplacementValue(eftnum, "xpath").getText();

				if (eftnumValue.equalsIgnoreCase(eftnoValue)) {

					String statusRow = replacementInLocator("PaymentHistory_status_xpath", replaceValuePay);
					String statusRowValue = getElementWithReplacementValue(statusRow, "xpath").getText();

					if (statusRowValue.equalsIgnoreCase("Stop Approval Pending")) {

						System.out.println("Status Row Value : " + statusRowValue);

						String updatedCogwheelLocator = replacementInLocator("PaymentHistoryCogwheelValue_xpath",
								replaceValuePay);
						getElementWithReplacementValue(updatedCogwheelLocator, "xpath").click();

						String optionAction = paymentHistory.get("Option_Value_2");
						System.out.println(optionAction);

						if (optionAction.equalsIgnoreCase("Approve")) {
							cogWheelOptions(optionAction);
							cogWheelOptionWindow();
							Thread.sleep(2000);
						}

						System.out.println("Switching to Main Window");
						driver.switchTo().defaultContent();
						Thread.sleep(5000);

						getElement("Save_xpath").click();
						Thread.sleep(2000);
						switchToFrame("tapestry");
						// to check status of EFT number.
						for (int eftRowCheck = 1; eftRowCheck < count; eftRowCheck++) {

							String replaceVal = String.valueOf(eftRowCheck);

							String eftnoRow = replacementInLocator("PaymentHistory_eft_xpath", replaceVal);

							Thread.sleep(3000);

							String eftnoRowValue = getElementWithReplacementValue(eftnoRow, "xpath").getText();
							System.out.println("Check No found:" + eftnoRowValue);

							if (eftnumValue.equals(eftnoRowValue)) {
								String statusSRow = replacementInLocator("PaymentHistory_status_xpath", replaceVal);
								String statusSRowValue = getElementWithReplacementValue(statusSRow, "xpath").getText();

								if (statusSRowValue.equalsIgnoreCase("Stopped")) {
									System.out.println("Status: " + statusRowValue + "And EFT No: " + eftnoRowValue);
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Method use for different cogwheel optoins
	public void cogWheelOptions(String optionAction) {
		String cogwheelValueOption = replacementInLocator("PaymentHistoryCogwheel_Options_xpath", optionAction);
		getElementWithReplacementValue(cogwheelValueOption, "xpath").click();
	}

	// Method is use to perform action on popup window
	public void cogWheelOptionWindow() throws InterruptedException {
		Thread.sleep(2000);

		popuphandler();		
		switchToDiv("PaymentHistory_Stop_ReasonCode_xpath");
		input("PaymentHistory_Stop_ReasonCodeInput_xpath", "Other");
		Thread.sleep(3000);
		click("PaymentHistory_Stop_GLIdentifier_xpath");
		getElement("PaymentHistory_Stop_Comments_xpath").click();
		Thread.sleep(2000);
		getElement("PaymentHistory_Stop_CommentsInput_xpath").sendKeys("Test");
		Thread.sleep(2000);
		getElement("PaymentHistory_Save_xpath").click();
	}

}
