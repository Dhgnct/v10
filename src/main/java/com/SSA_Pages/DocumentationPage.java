package main.java.com.SSA_Pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class DocumentationPage extends GenericClass {

	public void receivedDocuments(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			Thread.sleep(2000);
			switchToDiv("benefitsLink_xpath");
			Thread.sleep(2000);
			click("benefitsLink_xpath");
			System.out.println("Benefits segement Clicked once");
			Thread.sleep(2000);
			click("Edit_xpath");
			switchToFrame("tapestry");
			click("DocumentationTab_xpath");
			Thread.sleep(5000);
			click("ReceivedAllDocumentChkBox_xpath");
			Thread.sleep(5000);
			click("RequiredDocumentGrid_xpath");
			Thread.sleep(5000);
			click("ReceiveButton_xpath");
			Thread.sleep(5000);
			click("Ok_xpath");
			Thread.sleep(5000);
			documentStatus(testName);
			reportingPass(logger, "Receive document passed successfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Receive document failed", testName, nameofCurrMethod);
		}
	}

	public void documentStatus(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			HashMap<String, String> Documentdata = getData(Constants.INPUT_XLS, "Documentation", testName);
			List<WebElement> rowColl = getElements("PensionProofDocsTableRows_xpath");
			for (int row = 1; row <= rowColl.size(); row++) {

				String replaceValue = String.valueOf(row);
				String updatedLocator = replacementInLocator("ProofStatus_xpath", replaceValue);
				WebElement element = getElementWithReplacementValue(updatedLocator, "xpath");
				Select s = new Select(element);
				s.selectByVisibleText(Documentdata.get("Proof Status"));
				Thread.sleep(2000);
			}
			driver.switchTo().defaultContent();
			Thread.sleep(5000);
			click("Apply_xpath");
			Thread.sleep(2000);
			logger.log(LogStatus.INFO, "Document status updated successfully");
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Document status not updated successfully", testName, nameofCurrMethod);
		}
	}
}