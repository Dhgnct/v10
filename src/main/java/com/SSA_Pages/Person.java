package main.java.com.SSA_Pages;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.GenericClass.XLSReader;

public class Person extends GenericClass {

	public static Logger log = Logger.getLogger(Person.class.getName());
	XLSReader xlsObj = new XLSReader(Constants.INPUT_XLS);

	public void createPerson(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		String relationship = "";

		try {
			String[] relationshipSheet = { "QDRO", "Owner", "Custodian", "Guardian Ad Litem", "Guardian of the Estate",
					"Guardian of the Person", "Defendant", "Executor", "Attorney", "Trust", "Trustee", "Guardian",
					"Joint Ann", "PowerOfAttorney", "Beneficiary1", "Beneficiary2", "Beneficiary3" };

			int realtionshipSheetCount = relationshipSheet.length;
			for (int relationSheet = 0; relationSheet < realtionshipSheetCount; relationSheet++) {
				relationship = relationshipSheet[relationSheet];
				System.out.println("------------------"+relationship);
				HashMap<String, String> relationshipData = getData(Constants.INPUT_XLS, relationship, testName);
				if (!relationshipData.get("SSN").equals("")) {
					commonMethodAddPerson(relationshipData, testName, relationship );
					logger.log(LogStatus.INFO, "Successfully added" + relationship);
				}
			}
			reportingPass(logger, "Successfully created all the person related to test case", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Failed to create" + relationship, testName, nameofCurrMethod);
		}
	}
	
	public void commonMethodAddPerson(HashMap<String, String> relationshipData, String testName, String sheetName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			System.out.println("Enter into Create Person page");
			click("World_xpath");
			Thread.sleep(2000);
			switchToDiv("New_xpath");
			Thread.sleep(2000);
			WebElement elementToOperate = getElementinalist("NewList_xpath", "Person");
			elementToOperate.click();
			switchToFrame("tapestry");
			input("FirstName_xpath", relationshipData.get("First Name"));
			input("LastName_xpath", relationshipData.get("Last Name"));
			selectDDByVisibleText("SsnIndicator_id", relationshipData.get("SSN Indicator"));
			input("Ssn_id", relationshipData.get("SSN"));
			click("DateofBirth_xpath");
			input("DateofBirth_xpath", relationshipData.get("Date of Birth"));
			System.out.println(relationshipData.get("Marital Status"));
			selectDDByVisibleText("MaritalStatus_xpath", relationshipData.get("Marital Status"));
			AddAddress("Add Residential/Tax", testName, relationshipData); // Annuitant address details
			driver.switchTo().defaultContent();
			click("Apply_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			String alternateId = getElement("AlternateId_xpath").getAttribute("value");
			int rowNum = xlsObj.testCaseRow(sheetName, testName);
			xlsObj.setCellData(sheetName, "Alternate ID", rowNum, alternateId);
			driver.switchTo().defaultContent();
			click("Save_xpath");
			Thread.sleep(2000);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Failed to create" + sheetName, testName, nameofCurrMethod);
		}

	}
	
	public void createOrganisation(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		String relationship = "";

		try {
			String[] relationshipSheet = { "Beneficiary1", "Beneficiary2", "Beneficiary3" };

			int realtionshipSheetCount = relationshipSheet.length;
			for (int relationSheet = 0; relationSheet < realtionshipSheetCount; relationSheet++) {
				relationship = relationshipSheet[relationSheet];
				System.out.println("------------------"+relationship);
				HashMap<String, String> relationshipData = getData(Constants.INPUT_XLS, relationship, testName);
				if (!relationshipData.get("Organization Name").equals("")) {
					commonMethodAddOrganisation(relationshipData, testName, relationship );
					logger.log(LogStatus.INFO, "Successfully added" + relationship);
				}
			}
			reportingPass(logger, "Successfully created the organisation related to test case", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Failed to create" + relationship, testName, nameofCurrMethod);
		}
		    }
	
		public void commonMethodAddOrganisation(HashMap<String, String> relationshipData, String testName, String sheetName) throws Exception {
			
			String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

			try {
				System.out.println("Enter into Create Organisation page");
				click("World_xpath");
				Thread.sleep(2000);
				switchToDiv("New_xpath");
				Thread.sleep(2000);
				WebElement elementToOperate = getElementinalist("NewList_xpath", "Organization");
				elementToOperate.click();
				switchToFrame("tapestry");
				input("OrganizationName_xpath", relationshipData.get("Organization Name"));
				Thread.sleep(1000);
				click("AddressAdd_xpath");
				click("AddStreet_xpath");
				Thread.sleep(2000);
				popuphandler();
				Thread.sleep(2000);
				if (!relationshipData.get("Res_Zip").isEmpty()) {
					input("Residential/TaxAddress_Zip_xpath", relationshipData.get("Res_Zip") + Keys.TAB);
				}
				
				if (!relationshipData.get("Res_Address 1").isEmpty()) {
					input("Residential/TaxAddress_Address1_xpath", relationshipData.get("Res_Address 1"));
				}
				
				Thread.sleep(2000);
				switchToDiv("Residential/TaxAddressOk_xpath");
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				click("Apply_xpath");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				String alternateId = getElement("AlternateIdOrganisation_xpath").getAttribute("value");
				int rowNum = xlsObj.testCaseRow(sheetName, testName);
				xlsObj.setCellData(sheetName, "Alternate ID", rowNum, alternateId);
				driver.switchTo().defaultContent();
				click("Save_xpath");
				Thread.sleep(2000);

			} catch (Exception e) {
				e.printStackTrace();
				reportingFail(logger, "Failed to create" + sheetName, testName, nameofCurrMethod);
			}			
	}
	
	}

