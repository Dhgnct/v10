package main.java.com.SSA_Pages;

import java.io.IOException;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class FactoringPage extends GenericClass {
	
	
	//####################################--- Anees -------03142019------###################
	
	public String fnGetBenefitSegmentDetails(String testname) throws InterruptedException, IOException {
		String benefitsegment=null;
		System.out.println("Starting fnGetBenefitSegmentDetails");
		driver.switchTo().defaultContent();
		
		if(getElement("Edit_xpath").isDisplayed()) {
			click("Edit_xpath");
		}
		
		Thread.sleep(2000);
		click("benefitsLink_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		click("FactoringTab_id");
		Thread.sleep(2000);
		HashMap<String, String> facdata = getData(Constants.INPUT_XLS,"FactoringCo.1",testname);
		int totalFactoringCompany = getElements("FactoringCompanyAndBenefitSegmentCollection_xpath").size();
		for(int i=1;i<=totalFactoringCompany;i=i+2) {
			int j =i+1;
			String xpath1 = prop.getProperty("FactoringCompanyGrid_xpath");
			String xpath2="]";
			String factoringCompanyName = driver.findElement(By.xpath(xpath1 + i + xpath2)).getText();
			System.out.println("Anees : " + facdata.get("Organization Name"));
			if(factoringCompanyName.trim().equals(facdata.get("Organization Name").trim())) {
				benefitsegment = driver.findElement(By.xpath(xpath1 + j + xpath2)).getText();
			}
		}
		return benefitsegment;
	}
	

	
	//##################################----Nishith - 03142019-----#################################
	
	static String altId;
	static String BenefitSegmentdrpdwn;
	HashMap<String,String> factadata;
	
	public void createFactoringCo(String testName, int factoringCompany) throws Exception {

		factadata = getData(Constants.INPUT_XLS, "FactoringCo." + factoringCompany, testName);
		String nameofCurrMethod = new Throwable().getStackTrace()[0].getMethodName();

		logger = reports.startTest("Test-" + counter++ + ": Enter to create factoring", "Create Factoring co");
		logger.log(LogStatus.INFO, "Create Factoring Company");
		driver.switchTo().defaultContent();
		try {
			click("World_xpath");
			Thread.sleep(3000);
			click("New_xpath");
			WebElement elementtooperate = getElementinalist("NewList_xpath", "Factoring Co.");
			log.info("Click on Factoring Co.");
			elementtooperate.click();
			switchToFrame("tapestry");
			Thread.sleep(3000);
			input("TaxIdNum_xpath", factadata.get("Tax ID Number"));
			input("OrgName_xpath", factadata.get("Organization Name"));
			click("AddressCogwheel_xpath");
			click("AddStreetAddress_xpath");
			Thread.sleep(3000);
			popuphandler("Address Details");
			Thread.sleep(2000);
			input("Addrerss1_xpath", factadata.get("Street_Address 1"));
			input("Zip_xpath", factadata.get("Street_Zip"));
			click("City_xpath");
			Thread.sleep(3000);
			click("DoneButton_xpath");
			Thread.sleep(2000);
			popuphandler("Profile");
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Save_xpath");
			switchToFrame("tapestry");
			Thread.sleep(2000);
			altId = getElement("AlteranteId_xpath").getAttribute("value");
			Thread.sleep(2000);
			reportingPass(logger, "Factoring company created successfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Factoring company not created successfully", testName, nameofCurrMethod);
		}
	}
	

	

	public void addFactoring(String testName, int factoringCompany) throws Exception {

		String nameofCurrMethod = new Throwable().getStackTrace()[0].getMethodName();

		factadata = getData(Constants.INPUT_XLS, "Setup Factoring", testName);
		HashMap<String, String> foadata = getData(Constants.INPUT_XLS, "FOA 1", testName);
		HashMap<String, String> paymentData = getData(Constants.INPUT_XLS, "Payment Method", testName);

		logger = reports.startTest("Test-" + counter++ + ": add factoring company", "Add Factoring company");
		logger.log(LogStatus.INFO, "Add Factoring company");
		try {
			
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			click("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");

			
			 // click("PaymentSetupTab_xpath"); // click to payment setup Thread.sleep(2000);
			 
			BenefitSegmentdrpdwn = foadata.get("Basic Amount");

			// String BenefitSegmentdrpdwn2 =
			// getElement("BenefitAmount_xpath").getAttribute("id");

			System.out.println("benefit segment value: " + BenefitSegmentdrpdwn);

			/* In FOA 1 , ADD benefit */
			click("Factoring_xpath");
			Thread.sleep(3000);
			click("AddButton_xpath");
			Thread.sleep(3000);
			click("EditPayee_xpath");
			Thread.sleep(2000);
			
			// switchToTwoFrame("tapestry", "factorSearch"); //
			// v3-widget-SEARCH-v3page-window
			// selectDDByVisibleText("SelectFactRole_id", "Payee_Role_1");

			// switchToFrame("tapestry");
			// Thread.sleep(3000);
			// switchToDiv("factorDiv_xpath");
			
			popuphandler();
			Thread.sleep(2000);
			String altid = "123456789";
			click("factCoInput_xpath");
			clearValue("factCoInput_xpath");
			input("factCoInput_xpath", "Factoring Co.");
			enterKey();

			// input("FactsearchBox_xpath", altId);
			input("FactsearchBox_xpath", "R971842151");
			click("RolesearchBtn_xpath");
			Thread.sleep(2000);			
			switchToFrame("tapestry");
			
			explicitwait("periodStartdate_xpath", 20, "visibilityOfElementLocated");
			getElement("periodStartdate_xpath").sendKeys(Keys.HOME);
			String startDateVal=factadata.get("Start Date_" + factoringCompany);
			//String startDateVal=factadata.get("Start Date");
			input("periodStartdate_xpath", startDateVal); // change fr now
			//System.out.println("period date" + factadata.get("Start Date_" + factoringCompany));
			
			explicitwait("periodEndDate_xpath", 20, "visibilityOfElementLocated");
			getElement("periodEndDate_xpath").sendKeys(Keys.HOME);
			String endDateVal=factadata.get("End Date_" + factoringCompany);
			input("periodEndDate_xpath", endDateVal);
			Thread.sleep(2000);
			selectDDByVisibleText("PaymentMode_xpath", factadata.get("Pmt Mode_" + factoringCompany));
			Thread.sleep(2000);
			input("CourtOrderSplit_xpath", factadata.get("Petition/Court Order Split Amount_" + factoringCompany));
			
			if (!factadata.get("Remaining Amount ?_" + factoringCompany).equals("OFF")) {
				click("RemainAmtChkbx_xpath");
			}
			
			  Thread.sleep(2000);
			  selectDDByVisibleText("FactoringEndsDeath_xpath",factadata.get("Factoring ends at death?_" + factoringCompany));
			

			if (!factadata.get("Includes increases?_" + factoringCompany).equals("OFF")) {
				click("IncludeIncreases_xpath");
			}
			click("RecievedDate_xpath");
			input("RecievedDate_xpath", factadata.get("Received Date_" + factoringCompany));
			Thread.sleep(2000);
			selectByPartialVisibleText("benefit_tab_xpath", BenefitSegmentdrpdwn);
			Thread.sleep(2000);					

			selectDDByVisibleText("Status_xpath", factadata.get("Status_"+factoringCompany));
			Thread.sleep(2000);
			/*if (!factadata.get("Assignee_Count").equalsIgnoreCase("0")) {

				click("EditAssignee_xpath");
				popuphandler("Factor Assignee");
				Thread.sleep(2000);
				click("AddButton_xpath");
				Thread.sleep(2000);
				click("EditPayee_xpath");
				switchToFrame("factorSearch");
				input("FactorAssignee_Search_xpath", factadata.get("Assignee_Alternate ID_" + factoringCompany));
				click("FactorAssigneeButton_xpath");				
				driver.switchTo().defaultContent();
				Thread.sleep(2000);
				click("periodStartdate_xpath");
				input("periodStartdate_xpath", factadata.get("Assignee_Start Date_" + factoringCompany));

				click("periodEndDate_xpath");
				input("periodEndDate_xpath", factadata.get("End Date_" + factoringCompany));
				Thread.sleep(2000);
				input("FactorAssigneeAmount_xpath", ""); // amount to be discussed

				click("FactorAssigneeOkBtn_xpath");
			}*/

			editFactoringCompanyStatus(testName, "Open");
			/*
			  FactoringModellingToolPage FacToolModel = new FactoringModellingToolPage();
			  FacToolModel.FactoringModelingUIOrganisationTableData(testname, "Open");
			  editFactoringCompanyStatus(testname, "Court Order Received");
			  FacToolModel.FactoringModelingUIOrganisationTableData(testname,
			  "Court Order Received");
			 */

			reportingPass(logger, "Factoring company added successfully", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Factoring company not added successfully", testName, nameofCurrMethod);
		}
	}
	
	// ########################## Dhanraj #####################
	
	// Method use to update status of factoring company
	public void editFactoringCompanyStatus(String testname, String status) throws Exception{
		
		//popuphandler("Factoring");
		//driver.switchTo().defaultContent();
		
		if(!status.equalsIgnoreCase("Open")) {
			Thread.sleep(1000);
			click("editBtn_xpath");
			}
		//switchToFrame("tapestry");
		
		// condition matching two of conditions
		if(status.equalsIgnoreCase("Open") || status.equalsIgnoreCase("Court Order Received")) {
			System.out.println("------------"+status+"----------------");
			
			click("EditDocs_xpath");
			Thread.sleep(2000);
			//popuphandler("Proof Docs");
			
			//popuphandler();
			switchToFrame("PensionPages_FactoringProofDocsPopupPage");
			explicitwait("horizAdd_xpath", 20, "ElementToBeClickable");
			click("horizAdd_xpath");
			
			Thread.sleep(2000);
			click("AddDocs_xpath");
			Thread.sleep(2000);
			
			// Condition matches proper condition
			if(status.equalsIgnoreCase("Open")){
				selectDDByVisibleText("ProofDocumentDropdown_xpath", "Petition");
				
			}
			else if(status.equalsIgnoreCase("Court Order Received")) {
			selectDDByVisibleText("ProofDocumentDropdown_xpath", "Court Order");
			}
						
			click("OKButton_xpath");
			Thread.sleep(2000);
			
			click("CourtOderDate_xpath");
			input("CourtOderDate_xpath", factadata.get("Court Order Approval Date"));
			
			explicitwait("dcn_xpath", 30, "visibilityOfElementLocated");
			String valdcn="125";
			input("dcn_xpath",valdcn);
			
			click("OkBtn_DocProof_xpath");
			click("CloseDocsProof_xpath");
			//popuphandler("Factoring");
			switchToFrame("tapestry");
			
			//condition for change the status
			if(status.equalsIgnoreCase("Open")) {
				selectDDByVisibleText("Status_xpath","Open");
				click("Cogwheel_xpath");
				click("GenerateChecklist_xpath");
				
			}
			else {
				selectDDByVisibleText("Status_xpath","Court Order Received");
				}
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			click("Apply_xpath");
			enterKey();
			switchToFrame("tapestry");
			
			Thread.sleep(2000);
			
			//click("PopUpOK_xpath");
			
			driver.switchTo().defaultContent();
			click("Save_xpath");
			switchToFrame("tapestry");
			Thread.sleep(2000);
			//click("PopUpOK_xpath");
			}
	}
	
	//Number of factoring company according input sheet 
	public void createAndAddMultipleFactoringCompany(String testname) throws Exception {
		
		HashMap<String,String> factoringSetupData = getData(Constants.INPUT_XLS,"Setup Factoring",testname);
		String numberOfFactoringCompany = factoringSetupData.get("Factoring Count"); 
		int totalFactoringCompany = Integer.parseInt(numberOfFactoringCompany);
		
		for(int factoringCompany = 1; factoringCompany <= totalFactoringCompany; factoringCompany++) {
			
			//createFactoringCo(testname, factoringCompany);
			addFactoring(testname, factoringCompany);		
		}
	}	
	
	
	// this method is pre condition of adding factoring company- having pf steps
		// before adding factoring co. and after case activation
		public void pfStepsBeforeAddingFactoringCo(String testName) throws Exception {
			
			String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

			try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			explicitwait("Benefits_xpath", 10, "ElementToBeClickable");
			click("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");

			// for Submit for Good Order Review
			explicitwait("gearButtonBlueBanner_xpath", 10, "ElementToBeClickable");
			click("gearButtonBlueBanner_xpath");
			click("SubmitforGoodOrderReview_xpath");
			Thread.sleep(8000);
			driver.switchTo().defaultContent();
			switchToFrame("tapestry");
			if (isElementPresent("ConfirmMessagesOk_xpath")) {
				switchToDiv("ConfirmMessagesOk_xpath");
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				explicitwait("Edit_xpath", 20, "ElementToBeClickable");
			}
			else {
				driver.switchTo().defaultContent();
				explicitwait("Edit_xpath", 20, "ElementToBeClickable");
			}
			loginWithDifferentUserID(testName);
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			explicitwait("Benefits_xpath", 10, "ElementToBeClickable");
			click("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");

			// for Good Order Approved
			explicitwait("gearButtonBlueBanner_xpath", 10, "ElementToBeClickable");
			click("gearButtonBlueBanner_xpath");
			click("GoodOrderApproved_xpath");
			Thread.sleep(8000);
			driver.switchTo().defaultContent();
			switchToFrame("tapestry");
			if (isElementPresent("ConfirmMessagesOk_xpath")) {
				switchToDiv("ConfirmMessagesOk_xpath");
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			}
			else {
				driver.switchTo().defaultContent();
				explicitwait("Edit_xpath", 20, "ElementToBeClickable");
			}
			//Below two line use for call welcome package letter method
			BenefitsPage benefitsObj = new BenefitsPage();
			benefitsObj.verifyWelcomePackageLetter(testName);
			
			driver.switchTo().defaultContent();
			explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			click("Edit_xpath");
			explicitwait("Apply_xpath", 10, "ElementToBeClickable");
			switchToFrame("tapestry");
			Thread.sleep(5000);

			// for Certificate Issued
			explicitwait("gearButtonBlueBanner_xpath", 10, "ElementToBeClickable");
			click("gearButtonBlueBanner_xpath");
			click("CertificateIssued_xpath");
			Thread.sleep(8000);
			driver.switchTo().defaultContent();
			switchToFrame("tapestry");
			if (isElementPresent("ConfirmMessagesOk_xpath")) {
				switchToDiv("ConfirmMessagesOk_xpath");
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			}

			driver.switchTo().defaultContent();
			explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			reportingPass(logger, "pf Steps Before Adding FactoringCo is done successfully", testName, nameofCurrMethod);

		}
			catch (Exception e) {
				e.printStackTrace();
				reportingFail(logger, "Issue in pf Steps Before Adding FactoringCo", testName, nameofCurrMethod);
				functionStatus = "fail";
			}
		}
	
}
