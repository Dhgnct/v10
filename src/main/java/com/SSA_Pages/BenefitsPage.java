package main.java.com.SSA_Pages;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;

public class BenefitsPage extends GenericClass {
	
	DocumentationPage documentationPageObj = new DocumentationPage();

	public void benefitsHeaderAndSsDetails(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			logger.log(LogStatus.INFO, "Filling details on benefit page");
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			Thread.sleep(2000);
			switchToDiv("benefitsLink_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			explicitwait("NewBenefits_xpath", 10, "ElementToBeClickable");
			click("NewBenefits_xpath");
			Thread.sleep(2000);
			click("contractOKBtn_xpath");
			Thread.sleep(2000);
			HashMap<String, String> Benefitsdata = getData(Constants.INPUT_XLS, "Benefit_Details", testName);
			getElement("CommencementDateInput_xpath").sendKeys(Keys.HOME);
			input("CommencementDateInput_xpath", Benefitsdata.get("Annuity Commencement Date"));
			click("CalculateBtn_xpath");
			Thread.sleep(2000);
			input("OwnerState_xpath", Benefitsdata.get("Owner State"));
			clearValue("TotalPremium_xpath");
			input("TotalPremium_xpath", Benefitsdata.get("Total Premium"));
			popupError();
			Thread.sleep(2000);
			switchToFrame("tapestry");
			getElement("Exp.PremiumReciptDate_xpath").sendKeys(Keys.HOME);
			input("Exp.PremiumReciptDate_xpath", Benefitsdata.get("Exp. Premium Receipt Date"));
			click("PremiumReciptDate_xpath"); // Expected premium recevied date is auto
			// populated after AR is closed
			input("PremiumReciptDate_xpath", Benefitsdata.get("Premium Receipt Date"));

			if (!Benefitsdata.get("Assignment/Transfer Fee").isEmpty()) {
				input("Assgnment/TransferFee_xpath", Benefitsdata.get("Assignment/Transfer Fee"));
			}

			if (!Benefitsdata.get("Policy Fee").isEmpty()) {
				input("PolicyFee_xpath", Benefitsdata.get("Policy Fee"));
			}

			if (Benefitsdata.get("Rated Age").equals("ON")) {
				click("RatedAge_xpath");
			}
			if (Benefitsdata.get("Taxable").equals("ON")) {
				click("Taxable_xpath");
			}
			Thread.sleep(2000);
			driver.switchTo().parentFrame();
			click("Apply_xpath");
			addBenefitSegment(testName); // Adding benefit according excel sheet
			reportingPass(logger, "Benefit details entered successfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Benefit details not entered successfully", testName, nameofCurrMethod);
		}
	}

	// add Benefit Segment and it's Details
	public void addBenefitSegment(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			for (int foa = 1; foa <= 3; foa++) {				
				
				switchToFrame("tapestry");
				Thread.sleep(2000);
				scrollWindow("AddBenefitBtn_xpath");
				HashMap<String, String> foadata = getData(Constants.INPUT_XLS, "FOA " + foa, testName);
				System.out.println("Benefit segement add");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				System.out.println("*" + foadata.get("Segment Name") + "*");
				logger.log(LogStatus.INFO, foadata.get("Segment Name")+" is adding");
				if (foadata.get("Segment Name") == "") {
					break;
				} else {
					click("AddBenefitBtn_xpath");
					popuphandler();
					switchToFrame("PensionPages_BenefitSegments");
					Thread.sleep(2000);					
					input("Segment_xpath", foadata.get("Segment Name") + Keys.TAB);
					Thread.sleep(2000);
					input("StatReserve_xpath", foadata.get("Stat Reserve") + Keys.TAB);
					Thread.sleep(2000);
					click("PremiumAmount_xpath");
					input("PremiumAmount_xpath", foadata.get("Premium Amount"));
					click("SavePopup_id");
					click("close_xpath");
					Thread.sleep(3000);
					switchToFrame("tapestry");					
					Thread.sleep(2000);					
					commonMethodForFOADetails(foadata, testName);   // Method is use for filling Foa Details
					switchToFrame("tapestry");
					scrollWindow("BenefitSegments_Label_xpath");
					String replaceValue = String.valueOf(foa);
					String updatedLocator = replacementInLocator("Shrink_xpath", replaceValue);
					driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					getElementWithReplacementValue(updatedLocator, "xpath").click();
					Thread.sleep(2000);
					driver.switchTo().defaultContent();
					click("Apply_xpath");
					Thread.sleep(2000);
					logger.log(LogStatus.INFO, "Benefit segment "+foadata.get("Segment Name")+" added successfully", testName);
				}
			}
			driver.switchTo().defaultContent();
			click("Save_xpath");
			Thread.sleep(2000);
			reportingPass(logger, "Multiple benefit is added successfully",testName, nameofCurrMethod);
			
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Add Benefit method Unsuccessfully", testName, nameofCurrMethod);
		}
	}		

	// Method is common for filling data in FoA Details
	public void commonMethodForFOADetails(HashMap < String, String > foadata, String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try { 
		clearValue("BasicAmount_xpath");
		 Thread.sleep(3000);
		 input("BasicAmount_xpath", foadata.get("Basic Amount"));
		 selectDDByVisibleText("BaseForm_drpdown_xpath", foadata.get("Base Form"));
		 Thread.sleep(4000);
		 Actions action = new Actions(driver);
		 action.sendKeys(Keys.ENTER).build().perform();	
		 switchToFrame("tapestry");
		 if (foadata.get("Segment Name").equalsIgnoreCase("Lumpsum")) {
		  // This FOA is always One time Payment
		 } else {
		  selectDDByVisibleText("Frequency_xpath", foadata.get("Frequency"));
		 }

		 Thread.sleep(3000);
		 scrollWindow("OtherfeaturesChkBx_xpath");

		 if (foadata.get("Frequency").equalsIgnoreCase("Semi-Monthly")) {
		  selectDDByVisibleText("SM_Day1_xpath", foadata.get("SM_Day 1"));
		  selectDDByVisibleText("SM_Day2_xpath", foadata.get("SM_Day 2"));

		 } else if (foadata.get("Frequency").equalsIgnoreCase("Semi- Annual")) {
		  selectDDByVisibleText("SA_Day1_xpath", foadata.get("Day 1"));
		  selectDDByVisibleText("SA_Day2_xpath", foadata.get("Day 2"));
		 }

		 if (isElementPresent("PayeeBtn_xpath") && !foadata.get("Payee").isEmpty()) {
		  selectDDByVisibleText("PayeeBtn_xpath", foadata.get("Payee"));
		 }

		 if (isElementPresent("JointInfoBtn_xpath") && foadata.get("Joint Info").equalsIgnoreCase("ON")) {
		  selectDDByIndex("JointInfoTriggerdrpdwn_xpath", 1);
		  selectDDByIndex("JointAnnuitantdrpdwn_xpath", 1);
		  input("PercntRemainingatDeath_xpath", foadata.get("% Remaining at Death"));
		 }
		 // Code for Other Features section
		 scrollWindow("OtherfeaturesChkBx_xpath");
		 explicitwait("OtherfeaturesChkBx_xpath", 20, "visibilityOfElementLocated");
		 if (isElementPresent("OtherfeaturesChkBx_xpath") && foadata.get("Other Features").equalsIgnoreCase("ON")) {
		  click("OtherfeaturesChkBx_xpath");
		  selectDDByVisibleText("OtherFreatureTrigger_xpath", foadata.get("Other Features Trigger"));
		 }
		 
		 if (isElementPresent("CertainPeriod_xpath") && !foadata.get("Certain Period").isEmpty()) {
			   //getElement("CertainPeriod_xpath").sendKeys(Keys.HOME);
			   input("CertainPeriod_xpath", foadata.get("Certain Period"));
			  }
		 explicitwait("CertainPeriod_xpath", 20, "visibilityOfElementLocated");
		 
		  if (isElementPresent("CertainPeriod_xpath") && !foadata.get("Certain Period End Date").isEmpty()) {
		   getElement("CertainPeriodEndDate_xpath").sendKeys(Keys.HOME);
		   input("CertainPeriodEndDate_xpath", foadata.get("Certain Period End Date"));
		  }
		  if (isElementPresent("TemporaryPeriodEndDate_xpath") &&
		   !foadata.get("Temporary Period End Date").isEmpty()) {
		   input("TemporaryPeriodEndDate_xpath", foadata.get("Temporary Period End Date"));
		  }
		 

		 // Code for Reducing Death Benefit section

		 if (isElementPresent("ReducingDeathBenefit_xpath") &&
		  foadata.get("Reducing Death Benefit").equalsIgnoreCase("ON")) {
		  click("ReducingDeathBenefit_xpath");
		  //selectDDByVisibleText("ReducingDeathBenefitsTrigger_xpath", foadata.get("Reducing Death Benefit Trigger"));
		 }

		 if (isElementPresent("RdbCommencementDate_xpath") && !foadata.get("Rdb Commencement Date").isEmpty()) {
		  click("RdbCommencementDate_xpath");
		  Thread.sleep(5000);
		  input("RdbCommencementDate_xpath", foadata.get("Rdb Commencement Date"));
		 }

		 if (isElementPresent("RdbType_xpath") && !foadata.get("Rdb_Type").isEmpty()) {
		  selectDDByVisibleText("RdbType_xpath", foadata.get("Rdb_Type"));
		 }

		 if (isElementPresent("RefundType_xpath") && !foadata.get("Refund Type").isEmpty()) {
			 selectDDByVisibleText("RefundType_xpath", foadata.get("Refund Type"));
		 }

		 if (isElementPresent("InitialAmount_xpath") && !foadata.get("Initial Amount").isEmpty()) {
		  input("InitialAmount_xpath", foadata.get("Initial Amount"));
		 }

		 if (isElementPresent("RemainingBalance_xpath") && !foadata.get("Remaining Balance").isEmpty()) {
		  input("RemainingBalance_xpath", foadata.get("Remaining Balance"));
		 }
		 logger.log(LogStatus.INFO, "Benefit FOA details filled successfully");		
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Filling Benefit FOA details Unsuccessfully", testName, nameofCurrMethod);
		}
	}
	
	// Method is use for add secondary application
	public void addSecondaryApplication(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		
		try {
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Benefits_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		scrollWindow("RelatedApplicationLabel_xpath");
		Thread.sleep(2000);	
		click("RelatedApplicationHiddenOption_xpath");
		Thread.sleep(2000);
		click("RelatedApplicationAdd_xpath");
		Thread.sleep(2000);
		popuphandler("Secondary Applications");
		click("SecondaryApplicationSellectAllCheckBox_xpath");
		click("SecondaryApplicationCreateApplicationButton_xpath");
		Thread.sleep(2000);
		click("SecondaryApplicationSave_xpath");
		Thread.sleep(2000);
		click("SecondaryApplicationClose_xpath");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Save_xpath");
		Thread.sleep(2000);
		reportingPass(logger, "secondary application is added successfully", testName, nameofCurrMethod);
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to add secondary application", testName, nameofCurrMethod);
		}
	}
	
	// Method is use for secondary application Pf Steps
	public void secondaryAppPfSteps(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
		String mainWindow = driver.getWindowHandle();
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Benefits_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		scrollWindow("RelatedApplicationLabel_xpath");
		Thread.sleep(2000);

		int numberOfSecondaryApplicant = getElements("NumberOfSecondaryApplicant_xpath").size();
		logger.log(LogStatus.INFO,numberOfSecondaryApplicant+" is added as a secondary application");
		for (int secondaryApp = 1; secondaryApp <= numberOfSecondaryApplicant; secondaryApp++) {

			String replaceValue = String.valueOf(secondaryApp);
			String updatedsecondaryAppStatusLocator = replacementInLocator("SecondaryAppStatus_xpath", replaceValue);
			String secondaryAppStatus = getElementWithReplacementValue(updatedsecondaryAppStatusLocator, "xpath").getText();

			if (secondaryAppStatus.equalsIgnoreCase("Initial")) {

				String updatedGotoAppLinkLocator = replacementInLocator("SecondaryApplicationGotoAppLink_xpath",replaceValue);
				getElementWithReplacementValue(updatedGotoAppLinkLocator, "xpath").click();
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				switchToDiv("Edit_xpath");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("PfStepOption1_xpath"); // Generate Death Benefit Package
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("PfStepOption1_xpath"); // Move to Death Benefit in Process(S)
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("PfStepOption1_xpath"); // Beneficiary-joint Deceased
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				switchToDiv("Benefits_xpath");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("SecondaryPfStepReviewDocsProofs_xpath"); // Review Docs and Proofs
				Thread.sleep(2000);
				switchToFrame("tapestry");
				documentationPfStep(testName);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("SecondaryPfStepGeneratePaymentStream_xpath"); // Generate Payment Stream
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("SecondaryPfStepCreditCorrection_xpath"); // Credit Correction
				Thread.sleep(1000);
				popuphandler("Credit Correction");
				click("CreditCorrectionSave_xpath");
				Thread.sleep(2000);
				click("CreditCorrectionClose_xpath");
				Thread.sleep(2000);
				driver.switchTo().window(mainWindow);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("SecondaryPfStepSetupBankingAndDeductibleInfo_xpath"); // Set-up Banking And Deductible Info
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("PfStepsCowwheel_xpath");
				click("SecondaryPfStepSetupCompleteCreateApprovalWF_xpath"); // Set-up Complete Create Approval WF
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				clearValue("Search_xpath");
				search("Search_xpath", annuitantAltId("Annuitant", testName));
				Thread.sleep(2000);
				switchToFrame("tapestry");
				scrollWindow("RelatedApplicationLabel_xpath");
				Thread.sleep(2000);
				reportingPass(logger, "PF Steps for secondary application is failed", testName, nameofCurrMethod);
			}
		}
	}
	catch (Exception e) {
		e.printStackTrace();
		reportingFail(logger, "PF Steps for secondary application is done", testName, nameofCurrMethod);
	}
}
	
	// Method contains steps for Docs and proofs pf Steps
	public void documentationPfStep(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		
		try {
		click("ReceivedAllDocumentChkBox_xpath");
		Thread.sleep(5000);
		click("RequiredDocumentGrid_xpath");
		Thread.sleep(5000);
		click("ReceiveButton_xpath");
		Thread.sleep(5000);
		click("Ok_xpath");
		Thread.sleep(5000);
		documentationPageObj.documentStatus(testName);
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "PF Step for documentation is failed", testName, nameofCurrMethod);
		}
	}
	
	//Method contain PFSteps for CaseActivation
	public void caseActivationPFSteps(String testName) throws Exception {	
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		
		try {
		System.out.println("Starting of fnApproverPFSteps method ----- ");
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Benefits_xpath");
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		click("BlueBannerCogWheel_xpath");
		click("UpdateIndicativeData_xpath");	// Update Indicative Data
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Benefits_xpath");
		Thread.sleep(10000);
		switchToFrame("tapestry");
		click("BlueBannerCogWheel_xpath");
		click("QuoteDocumentReceived_xpath");	// Quote Document Received
		Thread.sleep(2000);
		click("BlueBannerCogWheel_xpath");
		click("PerformOFACCheck_xpath");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		click("Benefits_xpath");
		Thread.sleep(3000);
		switchToFrame("tapestry");
		click("BlueBannerCogWheel_xpath");
		click("CompleteAnnuitantsFOASetup_xpath");	//Complete Annuitants FOA Setup
		Thread.sleep(2000);
		click("BlueBannerCogWheel_xpath");
		click("RecalculateBenefitApp_xpath");	// Recalculate Benefit App
		Thread.sleep(2000);
		click("BlueBannerCogWheel_xpath");
		click("GeneratePaymentStream_xpath");	// Generate Payment Stream
		Thread.sleep(2000);
		/*PaymentSetupPage paymentSetupPageObj = new PaymentSetupPage();
		paymentSetupPageObj.addLienandLevies(testName);*/		//  calling different method of payment stream
		//click("Edit_xpath");
		
		/*PaymentSetupPage paymentSetupPageObj = new PaymentSetupPage();
		paymentSetupPageObj.QDROSplit(testName);*/
		
		System.out.println("code done till above Edit---AR approve ------ ");
		Thread.sleep(10000);
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");  // replace sleep from explicit wait in Oct
		click("BlueBannerCogWheel_xpath");
		click("Set-upBankingandDeductionInfo_xpath");	// Set-upBanking and Deduction Info	
		Thread.sleep(2000);		
		BenefitsPage benefitsObj = new BenefitsPage();
		benefitsObj.ProofOfAgeForAnnuitant(testName);
		Thread.sleep(10000);
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");	// Adding explicit wait in Oct
		click("BlueBannerCogWheel_xpath");
		click("Set-upComplete_xpath");		// Set-up Complete 
		System.out.println("code done till above save---AR approve ------ ");
		driver.switchTo().defaultContent();
		explicitwait("Edit_xpath", 20, "ElementToBeClickable");
		
		
		// Below code is use for Approve case activation from diffrent user
		loginWithDifferentUserID(testName);
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Benefits_xpath");
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("VerifyIndicativeData_xpath");
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		click("Benefits_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("VerifyOFACCheck_xpath");
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		click("Benefits_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("VerifyAnnuitantsFOASetup_xpath");
		Thread.sleep(12000);
		driver.switchTo().defaultContent();
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("VerifyBankingandDeductionInfo_xpath");
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("VerifyDocs_xpath");
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		explicitwait("BlueBannerCogWheel_xpath", 20, "ElementToBeClickable");
		click("BlueBannerCogWheel_xpath");
		click("CaseActivation_xpath");
		Thread.sleep(8000);
		driver.switchTo().defaultContent();
		switchToFrame("tapestry");
		switchToDiv("ConfirmMessagesOk_xpath");
		driver.switchTo().defaultContent();
		explicitwait("Edit_xpath", 20, "ElementToBeClickable");
		reportingPass(logger, "Case activated successfully", testName, nameofCurrMethod);
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Case is not activated", testName, nameofCurrMethod);
		}
	}
	
	public void ProofOfAgeForAnnuitant(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		
		try {
		driver.switchTo().defaultContent();
		explicitwait("Benefits_xpath", 10, "ElementToBeClickable");
		click("Benefits_xpath");
		explicitwait("Tapestry_xpath", 10, "FrameToBeAvailableAndSwitchToIt");
		switchToFrame("tapestry");
		click("DocumentationTab_xpath");
		Thread.sleep(2000);
		click("RequiredDocumentGrid_xpath");
		click("RequestDocument_xpath");
		selectDDByVisibleText("DocumentNameDropdown_xpath", "Commission Sharing Agreement");
		click("DocumentNameOkButton_xpath");
		explicitwait("RequiredDocumentGrid_xpath", 20, "ElementToBeClickable");
		
		
		List<WebElement> rowColl= getElements("PensionProofDocsTableRows_xpath");
		for(int row=1;row<=rowColl.size();row++) {
			
			String replaceValue = String.valueOf(row);
			String updatedLocator = replacementInLocator("DocumentName_xpath", replaceValue);	
			String documentName = getElementWithReplacementValue(updatedLocator, "xpath").getText();
			
			if(documentName.equalsIgnoreCase("Proof of Age for annuitant") || documentName.equalsIgnoreCase("Commission Sharing Agreement")) {
				
				String checkBoxUpdatedLocator = replacementInLocator("DocumentCheckBox_xpath", replaceValue);
				getElementWithReplacementValue(checkBoxUpdatedLocator, "xpath").click();
				click("RequiredDocumentGrid_xpath");
				Thread.sleep(5000);
				click("ReceiveButton_xpath");
				Thread.sleep(5000);
				click("Ok_xpath");
				String proofStatusUpdatedLocator = replacementInLocator("ProofStatus_xpath", replaceValue);	
				WebElement element = getElementWithReplacementValue(proofStatusUpdatedLocator, "xpath");
				Select s=new Select(element);
				s.selectByVisibleText("In Good Order");
				Thread.sleep(2000);
				driver.switchTo().defaultContent();
				click("Save_xpath");				
				explicitwait("Edit_xpath", 20, "ElementToBeClickable");
				click("Edit_xpath");
				explicitwait("Save_xpath", 20, "ElementToBeClickable");
				switchToFrame("tapestry");	
				
			}
		}
		click("ReceivedAllDocumentChkBox_xpath");
		click("RequiredDocumentGrid_xpath");
		click("AcceptSelected_xpath");
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDate localDate = LocalDate.now();
		input("AcceptSelectedInputField_xpath", dtf.format(localDate));
		click("AcceptedDateOkButton_xpath");
		explicitwait("RequiredDocumentGrid_xpath", 20, "ElementToBeClickable");
		driver.switchTo().defaultContent();
		Thread.sleep(5000);
		click("Apply_xpath");
		Thread.sleep(4000);
		reportingPass(logger, "Verification of Proof Of Age For Annuitant is done successfully", testName, nameofCurrMethod);
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Verification of Proof Of Age For Annuitant is failed", testName, nameofCurrMethod);
		}
	}
	
	//Method to update amount in existing FOA
		public void ModifyFOAAmount(String testName) throws Exception {
			
			try {		
			HashMap<String, String> FOAdata = getData(Constants.INPUT_XLS,"Benefit_Details", testName);
			
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			String mainWindow = driver.getWindowHandle();
			click("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("BenefitSegment_Cogwheel_xpath");
			click("BenefitSegment_ViewEditButton_xpath");
			popuphandler("View/Edit");
			getElement("BenefitsSegment_PremiumAmount_xpath").clear();		
			input("BenefitsSegment_PremiumAmount_xpath",FOAdata.get("Premium Amount"));
			Thread.sleep(2000);
			click("BenefitsSegment_SaveButton_xpath");
			Thread.sleep(2000);
			click("BenefitsSegment_CloseButton_xpath"); 
			Thread.sleep(5000);
			driver.switchTo().window(mainWindow);			
			Thread.sleep(2000);
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		//Method to Delete FOA
		public void DeleteFOA(String testName) throws Exception {
		
				try {
				String mainWindow = driver.getWindowHandle();	
				searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
				click("Benefits_xpath");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				
				click("BenefitSegment_Cogwheel_xpath");
				click("BenefitSegment_DeleteButton_xpath");		
				//driver.switchTo().defaultContent();
				driver.switchTo().window(mainWindow);
				click("Save_xpath");
				}
				catch(Exception e) {
					e.printStackTrace();
				}		
			} 
		
		// Method for verify welcome package letter
		public void verifyWelcomePackageLetter(String testName) throws Exception {

			boolean flag = false;
			String locatorOfWelcomePackageLetterStatus = "";
			String nameofCurrMethod = new Throwable().getStackTrace()[0].getMethodName();

			try {
				searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
				explicitwait("Benefits_xpath", 10, "ElementToBeClickable");
				click("Benefits_xpath");
				Thread.sleep(2000);
				switchToFrame("tapestry");
				click("DocumentationTab_xpath");

				int documentsRows = getElements("NumberofRows_xpath").size();
				for (int row = 1; row <= documentsRows; row++) {

					String replaceValue = String.valueOf(row);
					String nameUpdatedLocator = replacementInLocator("DocumentsName_xpath", replaceValue);
					String statusUpdatedLocator = replacementInLocator("DocumentsStatus_xpath", replaceValue);

					String name = getElementWithReplacementValue(nameUpdatedLocator, "xpath").getText();
					String status = getElementWithReplacementValue(statusUpdatedLocator, "xpath").getText();

					if (name.contains("Welcome Package Letter")) {

						flag = true; // it's verify that welcome package is present in row

						if (status.equalsIgnoreCase("Printed")) {
							locatorOfWelcomePackageLetterStatus = statusUpdatedLocator;
							break;
						}
					}
				}
				// Below code run if welcome package letter is present in document row
				if (flag == true) {
					boolean statusPrinted = true;
					// Below code run till status of welcome package status in Printed
					while (statusPrinted) {
						click("DocumentationTab_xpath");
						explicitwait("NumberofRows_xpath", 10, "visibilityOfElementLocated");
						String finalStatus = getElementWithReplacementValue(locatorOfWelcomePackageLetterStatus, "xpath")
								.getText();
						if (finalStatus.equalsIgnoreCase("Printed")) {
							statusPrinted = false;
							driver.switchTo().defaultContent();
							click("Save_xpath");
							switchToFrame("tapestry");
							System.out.println("welcome package letter status is in Printed");
							if (isElementPresent("ConfirmMessagesOk_xpath")) {
								switchToDiv("ConfirmMessagesOk_xpath");
								Thread.sleep(2000);
								driver.switchTo().defaultContent();
								explicitwait("Edit_xpath", 10, "ElementToBeClickable");
							} else {
								driver.switchTo().defaultContent();
								explicitwait("Edit_xpath", 20, "ElementToBeClickable");
							}
							explicitwait("Edit_xpath", 10, "ElementToBeClickable");
							break;
						} else {
							Thread.sleep(5000);
						}
					}

				} else {
					System.out.println("There is no any row having name of welcome package letter");
				}
				reportingPass(logger, "Welcome Package Letter generate successfully", testName, nameofCurrMethod);

			} catch (Exception e) {
				e.printStackTrace();
				reportingFail(logger, "Issue in verification of Welcome Package Letter generate successfully", testName, nameofCurrMethod);
				functionStatus = "fail";
			}
		}
		
		
}


