package main.java.com.SSA_Pages;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.GenericClass.XLSReader;

public class Annuitant extends GenericClass {

	public static Logger log = Logger.getLogger(Annuitant.class.getName());
	XLSReader xlsObj = new XLSReader(Constants.INPUT_XLS);
	

	public void createAnnuitant(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();		

		try {
			logger.log(LogStatus.INFO, "Staring createAnnuitant: ");
			System.out.println("Enter into Annuitant page");
			HashMap<String, String> createAnnuitantData = getData(Constants.INPUT_XLS, "Annuitant", testName);
			click("World_xpath");
			Thread.sleep(2000);
			switchToDiv("New_xpath");
			explicitwait("NewList_xpath", 10, "visibilityOfElementLocated");
			WebElement elementtoOperate = getElementinalist("NewList_xpath", "Annuitant");
			elementtoOperate.click();
			switchToFrame("tapestry");
			selectDDByVisibleText("Prefix_xpath", createAnnuitantData.get("Prefix"));
			input("FirstName_xpath", createAnnuitantData.get("First Name"));
			input("LastName_xpath", createAnnuitantData.get("Last Name"));
			selectDDByVisibleText("SsnIndicator_id", createAnnuitantData.get("SSN Indicator"));
			input("Ssn_id", createAnnuitantData.get("SSN"));
			click("DateofBirth_xpath");
			input("DateofBirth_xpath", createAnnuitantData.get("Date of Birth"));
			selectDDByVisibleText("Gender_xpath", createAnnuitantData.get("Gender"));
			System.out.println(createAnnuitantData.get("Marital Status"));
			selectDDByVisibleText("MaritalStatus_xpath", createAnnuitantData.get("Marital Status"));
			AddAddress("Add Residential/Tax", testName, createAnnuitantData);
			driver.switchTo().defaultContent();

			// calling contract&cert
			ContractsAndCerts contractsAndCertsObj = new ContractsAndCerts();
			contractsAndCertsObj.createContractAndCerts(testName);
			driver.switchTo().defaultContent();
			click("Edit_xpath");
			Thread.sleep(2000);
			click("Profile_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			explicitwait("AlternateId_xpath", 20, "visibilityOfElementLocated");
			String alternateId = getElement("AlternateId_xpath").getAttribute("value");
			int rowNum = xlsObj.testCaseRow("Annuitant", testName);
			xlsObj.setCellData("Annuitant", "Alternate ID", rowNum, alternateId);
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			click("Save_xpath");
			Thread.sleep(2000);
			reportingPass(logger, "Annuitant create successfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Error in Annuitant creation", testName, nameofCurrMethod);
			functionStatus = "fail";
		}
	}

	// Method is use for perform death of annuitanr and verify death of annuitant
	public void executedDeathAndVerification(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
		HashMap<String, String> annuitantDeathData = getData(Constants.INPUT_XLS, "Annuitant", testName);
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		click("Profile_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		getElement("DateOfDeath_xpath").sendKeys(Keys.HOME);
		input("DateOfDeath_xpath", annuitantDeathData.get("Date of Death"));
		driver.switchTo().defaultContent();
		switchToDiv("Apply_xpath");
		Thread.sleep(1000);
		popuphandler();
		if (isElementPresent("DeathConfirmationPopupOkButton_xpath")) {
			System.out.println("Death conifirmation popup");
			click("DeathConfirmationPopupOkButton_xpath");
		}

		Thread.sleep(2000);
		popupError();
		switchToDiv("Save_xpath");
		popupError();
		explicitwait("StatusDeceadesOnBanner_xpath", 60, "visibilityOfElementLocated");
		if (isElementPresent("StatusDeceadesOnBanner_xpath")) {
			switchToDiv("Benefits_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("PaymentSetup_xpath");
			Thread.sleep(2000);
			String lightGreenBannerText = getElement("LightGreenBannerText_xpath").getAttribute("innerText");
			if (lightGreenBannerText.contains("Deceased")) {
				int numberOfPaymentStream = getElements("NumberOfPaymentStream_xpath").size();
				System.out.println("No. of Payment Stream = " + numberOfPaymentStream);
				for (int paymentStream = 1; paymentStream <= numberOfPaymentStream; paymentStream++) {
					switchToDiv("PaymentStreamDiv_xpath");
					Thread.sleep(2000);
					String replaceValue = String.valueOf(paymentStream);
					String updateLocator = replacementInLocator("PaymentStreamPaymentStatus_xpath", replaceValue);
					String paymentStreamStatusValue = getElementWithReplacementValue(updateLocator, "xpath")
							.getAttribute("value");
					System.out.println(paymentStreamStatusValue);
					if (paymentStreamStatusValue.equalsIgnoreCase("Complete")) {
						System.out.println(paymentStream + " Payment status complete");
						continue;
					} else {
						Assert.fail();
						break;
					}
				}
			} else {
				Assert.fail();
			}
		} else {
			Assert.fail();
		}
	}
		catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Error in Annuitant creation", testName, nameofCurrMethod);
		}
	}	
	
	// Method is use for changes in Annuitant profile
	public void DemographChange(String testName) throws Exception {

        System.out.println("demography updates");
        HashMap<String, String> demoData = getData(Constants.INPUT_XLS, "Update Demo", testName);
        searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
        
        switchToFrame("tapestry");
        selectDDByVisibleText("Prefix_xpath", demoData.get("Prefix"));
        clearValue("FirstName_xpath");
        input("FirstName_xpath", demoData.get("First Name"));
        clearValue("LastName_xpath");
        input("LastName_xpath", demoData.get("Last Name"));
        Thread.sleep(2000);        
        getElement("DateofBirth_xpath").sendKeys(Keys.HOME);
        input("DateofBirth_xpath", demoData.get("Date of Birth"));
        selectDDByVisibleText("Gender_xpath", demoData.get("Gender"));        
        driver.switchTo().defaultContent();
        click("Apply_xpath");
        Thread.sleep(2000);
        switchToFrame("tapestry");
        switchToDiv("OK1_xpath"); 
        Thread.sleep(2000);        
        driver.switchTo().defaultContent();
        Thread.sleep(4000);
        click("Save_xpath");
        Thread.sleep(4000);
        
 }
 
}
