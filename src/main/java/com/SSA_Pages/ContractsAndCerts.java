package main.java.com.SSA_Pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.GenericClass.XLSReader;

public class ContractsAndCerts extends GenericClass {
	public static Logger log = Logger.getLogger(ContractsAndCerts.class.getName());
	XLSReader xlsObj = new XLSReader(Constants.INPUT_XLS);

	public void createContractAndCerts(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		HashMap<String, String> Contractsandcertsdata = getData(Constants.INPUT_XLS, "Search OWN-Link", testName);
		System.out.println("contract page");
		
		try {
			logger.log(LogStatus.INFO, "Fill details on contract and certs page");				
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			switchToDiv("ContractsCerts_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("ContractsCertsAdd_xpath");
			switchToTwoFrame("tapestry", "memberContractSearch");
			input("GroupIDNumber_xpath", Contractsandcertsdata.get("Group ID Number"));
			Thread.sleep(2000);
			click("C&C-Search_xpath");
			driver.switchTo().defaultContent();
			switchToFrame("tapestry");
			Thread.sleep(2000);
			selectDDByIndex("PlanStructuredProduct_xpath", 1);
			Thread.sleep(2000);
			selectDDByVisibleText("CertificateRecipient_xpath", Contractsandcertsdata.get("Certificate Recipient"));
			selectDDByVisibleText("StateforPremiumTaxes_xpath", Contractsandcertsdata.get("State for Premium Taxes"));
			popuphandler("Contracts & Certs");
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(2000);
			reportingPass(logger, "contract created succcessfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Error in contract creation", testName, nameofCurrMethod);
			functionStatus = "fail";			
		}
	}

	public void addRelationShip(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();
		String relationnType = "";
		try {
			String[] relationshipSheet = { "QDRO", "Owner", "Custodian", "Guardian Ad Litem", "Guardian of the Estate",
					"Guardian of the Person", "Defendant", "Executor", "Attorney", "Trust", "Trustee", "Guardian",
					"Joint Ann", "PowerOfAttorney", "Beneficiary1", "Beneficiary2", "Beneficiary3" };

			int realtionshipSheetCount = relationshipSheet.length;
			for (int realtionSheet = 0; realtionSheet < realtionshipSheetCount; realtionSheet++) {

				relationnType = relationshipSheet[realtionSheet];
				HashMap<String, String> relationshipData = getData(Constants.INPUT_XLS, relationnType, testName);
				if (relationnType.contains("Beneficiary")) {
					logger.log(LogStatus.INFO, "Relation Type is Beni");
					if (!relationshipData.get("Alternate ID").equals("")) {
						commonMethodAddRelationship(relationshipData, testName);
						beneficiaryElections(testName, relationnType);
						click("Save_xpath");
						Thread.sleep(2000);
					}
				} else if (relationnType.contains("QDRO")) {
					logger.log(LogStatus.INFO, "Relation Type is QDRO");
					if (!relationshipData.get("Alternate ID").equals("")) {
						commonMethodAddRelationship(relationshipData, testName); // ------------ editing
						DRODetails(relationnType, testName);
						explicitwait("Save_xpath", 10, "ElementToBeClickable");
						click("Save_xpath");
						Thread.sleep(2000);
					}
				} else {
					if (!relationshipData.get("Alternate ID").equals("")) {
						commonMethodAddRelationship(relationshipData, testName);
						explicitwait("Save_xpath", 10, "ElementToBeClickable");
						click("Save_xpath");
						Thread.sleep(2000);
					}
				}
			}
			reportingPass(logger, "Relationship added successfully", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to add" + relationnType, testName, nameofCurrMethod);
			functionStatus = "fail";
		}
	}

	// This method is used in addRelation method
	public void commonMethodAddRelationship(HashMap<String, String> sheetData, String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("ContractsCerts_xpath");
			switchToFrame("tapestry");
			System.out.println("Adding relationship details");
			Thread.sleep(2000);
			click("AddRelation_xpath");
			Thread.sleep(2000);
			popuphandler();
			explicitwait("RelationshipDD_xpath", 20, "visibilityOfElementLocated");
			switchToDiv("RelationshipDD_xpath");
			explicitwait("RelationshipDD_xpath", 10, "visibilityOfElementLocated");
			input("RelationshipDD_xpath", sheetData.get("Relationship"));
			Thread.sleep(2000);
			input("RelationshipDD_xpath", "\n");
			explicitwait("InputRelation_xpath", 20, "visibilityOfElementLocated");
			input("InputRelation_xpath", sheetData.get("Alternate ID"));
			Thread.sleep(2000);
			input("InputRelation_xpath", "\n");
			System.out.println("relation searched");
			Thread.sleep(2000);
			switchToDiv("SelectRelationAfterSearch_xpath");
			Thread.sleep(2000);
			explicitwait("EntityRelationshipEffectiveDate_xpath", 20, "visibilityOfElementLocated");	
			
			if (getElement("EntityRelationshipEffectiveDate_xpath").getAttribute("value").equals("")) {
				getElement("EntityRelationshipEffectiveDate_xpath").sendKeys(Keys.HOME);
				input("EntityRelationshipEffectiveDate_xpath", sheetData.get("Effective Date"));			
				click("Next_xpath");
				Thread.sleep(2000);
			}
			
			else {
				
				click("Next_xpath");
				Thread.sleep(2000);
			} 
			
		/*	if(getElement("BeneCheckLabel_xpath").getAttribute("title").equalsIgnoreCase("Beneficiary"))
			{
				click("Next_xpath");
				Thread.sleep(2000);
			}
			
			else {
				
				explicitwait("EntityRelationshipEffectiveDate_xpath", 20, "visibilityOfElementLocated");
				getElement("EntityRelationshipEffectiveDate_xpath").sendKeys(Keys.HOME);
				click("Next_xpath");
				Thread.sleep(2000);
			}*/
			
			
			click("Confirm_xpath");
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			explicitwait("Save_xpath", 10, "ElementToBeClickable");
			click("Save_xpath");
			logger.log(LogStatus.INFO, "Relationship is searched and selected successfully");
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to search and select relationship", testName, nameofCurrMethod);
			functionStatus = "fail";
		}
	}

	public void beneficiaryElections(String testName, String sheetName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			System.out.println("Beneficiary Election");

			HashMap<String, String> beniElectionData = getData(Constants.INPUT_XLS, sheetName, testName);
			popuphandler("Contracts & Certs");
			Thread.sleep(3000);
			switchToFrame("tapestry");
			explicitwait("BeniElection_xpath", 20, "visibilityOfElementLocated");
			click("BeniElection_xpath");
			getElement("ElectionEffectiveDate_xpath").sendKeys(Keys.HOME);
			input("ElectionEffectiveDate_xpath", beniElectionData.get("Effective date"));
			click("Election_okbutton_xpath");
			Thread.sleep(2000);
			popuphandler();
			switchToFrame("WorldPages_BeneficiaryNewElectionPage");
			Thread.sleep(2000);
			switchToDiv("NewElectionsShowHide_xpath");
			Thread.sleep(1000);
			click("AUTO_ASSIGN_xpath");
			click("BeniOk_xpath");
			logger.log(LogStatus.INFO, "Beneficiary Election is done successfully");
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to Beneficiary Election ", testName, nameofCurrMethod);
			functionStatus = "fail";
		}
	}

	public void addBroker(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			System.out.println("Add Broker Records");
			HashMap<String, String> AddBrokerData = getData(Constants.INPUT_XLS, "Add Broker", testName);
			int broker = 0;
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("ContractsCerts_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");

			String brokerCount = AddBrokerData.get("Broker Count").replace(".0", "");
			int bCount = Integer.parseInt(brokerCount);
			System.out.println("Number of broker =" + bCount);
			Thread.sleep(2000);

			try {
				for (broker = 1; broker <= bCount; broker++) {

					logger.log(LogStatus.INFO, "Total number of Broker is" + bCount);
					click("ShowHideBroker_xpath");
					Thread.sleep(2000);
					scrollWindow("DRODetailsText_xpath");
					click("AddBroker_xpath");
					switchToTwoFrame("tapestry", "search");

					if (!AddBrokerData.get("Broker ID_" + broker).isEmpty()) {
						Thread.sleep(3000);
						System.out.println("Add Broker data...." + AddBrokerData.get("Broker ID_" + broker));
						switchToDiv("SearchBroker_xpath");
						input("SearchBroker_xpath", AddBrokerData.get("Broker ID_" + broker));
					}

					click("SearchButton_xpath");
					Thread.sleep(2000);
					click("SearchResultGrid_xpath");
					Thread.sleep(2000);
					switchToFrame("tapestry");
					switchToDiv("BrokerageGridFielset_xpath");
					Thread.sleep(2000);
					logger.log(LogStatus.INFO, broker + " Added successfully");
				}
			} catch (Exception e) {
				e.printStackTrace();
				reportingFail(logger, "Fail to add " + broker + " particular Broker", testName, nameofCurrMethod);
				functionStatus = "fail";
			}
			click("ShowHideBroker_xpath");
			Thread.sleep(1000);
			click("BrokerAdminAutoAssign_xpath");
			popuphandler("Contracts & Certs");
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(2000);

			if (broker == bCount) {
				reportingPass(logger, bCount + " Added successfully", testName, nameofCurrMethod);
			}

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to add Broker ", testName, nameofCurrMethod);
			functionStatus = "fail";
		}
	}

	public void pfSteps_ValidateBroker_GeanerateLokin(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			click("ContractsCerts_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("Contract&CertsCogwheel_xpath");
			click("GenerateMissingDocsLetter_xpath"); // GenerateMissingDocsLetter_xpath
			driver.switchTo().alert().accept();
			Thread.sleep(7000);
			switchToFrame("tapestry");
			BenefitsPage benefitsPageObj = new BenefitsPage();
			benefitsPageObj.documentationPfStep(testName); // Proofs and Docs validation
			click("ContractsCerts_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("Contract&CertsCogwheel_xpath");
			click("VerifyBrokerLicense_xpath"); // Verify Broker License
			driver.switchTo().alert().accept();
			Thread.sleep(7000);
			switchToFrame("tapestry");
			explicitwait("Contract&CertsCogwheel_xpath", 10, "ElementToBeClickable");
			click("Contract&CertsCogwheel_xpath");
			click("GenerateLock-inNumber_xpath"); // Generate Lock IN
			driver.switchTo().alert().accept();
			Thread.sleep(7000);
			driver.switchTo().defaultContent();
			explicitwait("Apply_xpath", 20, "ElementToBeClickable");
			click("Apply_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			String lockIn = getElement("Cert/Lock-InValue_xpath").getAttribute("value");
			int rowNum = xlsObj.testCaseRow("Add Broker", testName);
			xlsObj.setCellData("Add Broker", "Lockin ID", rowNum, lockIn);
			System.out.println("Alt Id after lokin update = " + annuitantAltId("Annuitant", testName));
			driver.switchTo().defaultContent();
			click("Save_xpath");
			Thread.sleep(2000);
			reportingPass(logger, "Validation of Broker and Geanerate Lock-in is done successfully", testName, nameofCurrMethod);

		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to Validate Broker and Geanerate Lock-in", testName, nameofCurrMethod);
		}
	}

	// adding DRO details---------arun 885522104
	public void DRODetails(String sheetName, String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit(sheetName, testName);
			switchToDiv("ContractsCerts_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			Thread.sleep(2000);
			System.out.println("Adding DRO details");
			HashMap<String, String> AddDroDetails = getData(Constants.INPUT_XLS, "QDRO", testName);
			scrollWindow("scroll_xpath");
			Thread.sleep(2000);
			click("showHide_xpath");
			Thread.sleep(2000);
			click("Addbutton_xpath");
			Thread.sleep(2000);
			input("Casenumber_xpath", AddDroDetails.get("Case Number"));
			Thread.sleep(2000);
			selectDDByVisibleText("CourtOrderType_xpath", AddDroDetails.get("Court Order Type"));
			getElement("CourtOrderDate_xpath").sendKeys(Keys.HOME);
			Thread.sleep(2000);
			input("CourtOrderDate_xpath", AddDroDetails.get("Court Order Date"));

			if (isElementPresent("SplitType_xpath") && AddDroDetails.get("Split Type").equals("Percent")) {
				selectDDByVisibleText("SplitType_xpath", AddDroDetails.get("Split Type"));
				Thread.sleep(2000);
				input("Percentage_xpath", AddDroDetails.get("Percentage"));

			}
			if (isElementPresent("SplitType_xpath") && AddDroDetails.get("Split Type").equals("Amount")) {
				selectDDByVisibleText("SplitType_xpath", AddDroDetails.get("Split Type"));
				Thread.sleep(2000);
				input("FlatAmount_xpath", AddDroDetails.get("Flat Amount"));
			}
			Thread.sleep(2000);
			selectDDByVisibleText("DeathOfPayee_xpath", AddDroDetails.get("On Death of Payee"));
			Thread.sleep(2000);
			selectDDByVisibleText("DeathOfPrimaryAnnuitant_xpath", AddDroDetails.get("On Death of Primary Annuitant"));
			Thread.sleep(2000);

			if (isElementPresent("EligibleForIncrease_xpath")
					&& AddDroDetails.get("Eligible for Increase").equals("ON")) {
				click("EligibleForIncrease_xpath");

			}
			driver.switchTo().parentFrame();
			reportingPass(logger, "DRO details are added successfully", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Fail to add DRO details ", testName, nameofCurrMethod);
		}
	}

	public void brokerCommissionValidation(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		boolean flag = false;

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", "CI_TC_8");
			explicitwait("Apply_xpath", 10, "visibilityOfElementLocated");
			click("ContractsCerts_xpath");
			switchToFrame("tapestry");
			click("Contract&CertsCogwheel_xpath");
			click("TriggerCommission_xpath");
			acceptAlert();
			Thread.sleep(6000);
			driver.switchTo().defaultContent();
			explicitwait("Save_xpath", 10, "ElementToBeClickable");
			click("Save_xpath");
			explicitwait("Edit_xpath", 10, "visibilityOfElementLocated");
			click("SideArrowNavigator_id");
			explicitwait("SideArrowRefreshButton_xpath", 5, "ElementToBeClickable");
			System.out.println("update code");
			click("SideArrowRefreshButton_xpath");
			Thread.sleep(3000);
			int rowLength = getElements("ShowWflowsTypeRows_xpath").size();
			for (int row = 1; row <= rowLength; row++) {

				String replaceValue = String.valueOf(row);
				String updateLocator = replacementInLocator("ShowWflowsTypeText_xpath", replaceValue);
				String type = getElementWithReplacementValue(updateLocator, "xpath").getText();
				if (type.equalsIgnoreCase("Broker Commission Approval")) {
					flag = true;
					click("SideArrowNavigator_id");
					reportingPass(logger, "Broker commission workflow is generated", testName, nameofCurrMethod);
					break;
				}
			}
			if (flag != true) {
				reportingFail(logger, "Broker commission workflow is not generated", testName, nameofCurrMethod);
			} else {
				reportingPass(logger, "Broker commission workflow is generated successfully", testName, nameofCurrMethod);
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Broker commission workflow is not generated", testName, nameofCurrMethod);
		}
	}

	public void updateRelationshipFirstName(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("ContractsCerts_xpath");
			switchToFrame("tapestry");
			click("Relationships1stRowGear_xpath");
			explicitwait("Relationships1stRowGearDetails_xpath", 10, "ElementToBeClickable");
			click("Relationships1stRowGearDetails_xpath");
			popuphandler();
			explicitwait("RelationshipsContactsFirstName_xpath", 10, "visibilityOfElementLocated");
			clearValue("RelationshipsContactsFirstName_xpath");
			input("RelationshipsContactsFirstName_xpath", "Dhanraj");
			click("RelationshipsContactsSave_xpath");
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			click("Save_xpath");
			explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			reportingPass(logger, "Update Relationship First Name successfully", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Relationship First Name is not updated successfully", testName, nameofCurrMethod);
		}

	}

	public void updateRelationshipStopDate(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			switchToDiv("ContractsCerts_xpath");
			switchToFrame("tapestry");
			click("Relationships1stRowGear_xpath");
			explicitwait("Relationships1stRowGearDetails_xpath", 10, "ElementToBeClickable");
			click("Relationships1stRowGearDetails_xpath");
			popuphandler();
			explicitwait("RelationshipsContactsStopDate_xpath", 10, "visibilityOfElementLocated");
			clearValue("RelationshipsContactsStopDate_xpath");
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			LocalDateTime now = LocalDateTime.now();
			input("RelationshipsContactsStopDate_xpath", dtf.format(now));
			click("RelationshipsContactsSave_xpath");
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			click("Save_xpath");
			explicitwait("Edit_xpath", 10, "ElementToBeClickable");
			reportingPass(logger, "Update Relationship Stop Date successfully", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Relationship Stop Date is not updated successfully", testName, nameofCurrMethod);
		}
	}
}
