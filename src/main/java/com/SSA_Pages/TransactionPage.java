package main.java.com.SSA_Pages;

import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;

import main.java.com.GenericClass.Constants;
import main.java.com.GenericClass.GenericClass;
import main.java.com.login.Login;

public class TransactionPage extends GenericClass {

	public void incomingARclosed(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			HashMap<String, String> Transactiondata = getData(Constants.INPUT_XLS, "Received_Premium", testName);
			HashMap<String, String> AddBrokerData = getData(Constants.INPUT_XLS, "Add Broker", testName);
			System.out.println("Payment Receving");
			clearValue("Search_xpath");
			search("Search_xpath", "incoming");
			click("FirstRowAfterSearchIncomingPremium_xpath");
			Thread.sleep(2000);
			String mainWindow = driver.getWindowHandle();
			click("Edit_xpath");
			explicitwait("Cancel_xpath", 20, "visibilityOfElementLocated");
			switchToFrame("tapestry");
			click("TransactionAnchor_xpath");
			explicitwait("PaymentsHeader_xpath", 50, "visibilityOfElementLocated");
			click("ShowHiddenHR_xpath");
			Thread.sleep(2000);
			click("CogwheelTransaction_xpath");
			Thread.sleep(2000);
			click("PaymentReceived_xpath");
			Thread.sleep(2000);
			popuphandler();
			switchToFrame("TransactionPages_PremiumPaymentPopupPage");
			click("PaymentReceivedShowHiddenHR_xpath");
			Thread.sleep(1000);
			input("BatchNoTransaction_xpath", Transactiondata.get("Batch No") + Keys.TAB);
			System.out.println(Transactiondata.get("Batch No"));
			input("Check/wireTransaction_xpath", Transactiondata.get("Check/Wire No") + Keys.TAB);
			System.out.println(Transactiondata.get("Check/Wire No"));
			selectDDByVisibleText("PaymentReceivedPaymentType_xpath", Transactiondata.get("Premium Received Type"));
			input("PaymentReceivedAmount_xpath", Transactiondata.get("Premium Received Amount"));
			click("PaymentReceivedSave_xpath");
			explicitwait("PaymentReceivedClose_xpath", 10, "ElementToBeClickable");
			click("PaymentReceivedClose_xpath");
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			click("Apply_xpath");
			switchToFrame("tapestry");
			click("TransactionAnchor_xpath");
			explicitwait("PaymentsHeader_xpath", 20, "visibilityOfElementLocated");
			click("PaymentARFirstRowCogweel_xpath");
			click("PaymentARMove_xpath");
			Thread.sleep(2000);
			popuphandler("Search & Move Payments");
			click("MatchA/RItems_xpath"); 
			Thread.sleep(2000);
			input("CertLockIn_xpath", AddBrokerData.get("Lockin ID"));
			click("PremiumReceivedSearch_xpath");
			Thread.sleep(2000);
			click("MatchingContractsReceivablesCogweel_xpath");
			click("MatchingContractsReceivablesPaymentAmountMove_xpath");
			acceptAlert();
			Thread.sleep(2000);
			click("SelectToApply_xpath");
			click("DebitTransactionCheckBox_xpath");
			click("CreditItemsSave_xpath");
			Thread.sleep(2000);
			click("CreditItemsClose_xpath");
			Thread.sleep(2000);
			driver.switchTo().window(mainWindow);
			click("Apply_xpath");
			Thread.sleep(1000);
			click("Save_xpath");
			Thread.sleep(2000);
			logger.log(LogStatus.INFO, "AR closed with same user");
			arApprovedByDifferentUser(testName, Transactiondata);	
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(5000);
			reportingPass(logger, "AR Closed successfully form Approver", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Issue in AR Close", testName, nameofCurrMethod);
		}
	}

	public void annuitantARClosed(String testName) throws Exception {
		
		String nameofCurrMethod = new Throwable() .getStackTrace()[0].getMethodName();

		try {
			Login loginObj = null;
			loginObj = PageFactory.initElements(driver, Login.class);
			HashMap<String, String> Transactiondata = getData(Constants.INPUT_XLS, "Received_Premium", testName);
			System.out.println("Annuitant level Payment Receving");
			searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
			String mainWindow = driver.getWindowHandle();
			click("Contracts&Certs_xpath");
			Thread.sleep(2000);
			switchToFrame("tapestry");
			click("C&CTransactions_xpath");
			explicitwait("TransactionCogwheel_xpath", 10, "ElementToBeClickable");
			click("TransactionCogwheel_xpath");
			click("TransactionPaymentRowCogwheelPayments_xpath");
			Thread.sleep(2000);
			popuphandler();
			switchToFrame("WorldPages_PaymentPage");
			switchToDiv("PaymentsShoWHide_xpath");
			Thread.sleep(2000);
			click("PaymentsAdd_xpath");
			Thread.sleep(2000);
			input("BatchNoTransaction_xpath", Transactiondata.get("Batch No") + Keys.TAB);
			System.out.println(Transactiondata.get("Batch No"));
			input("Check/wireTransaction_xpath", Transactiondata.get("Check/Wire No") + Keys.TAB);
			System.out.println(Transactiondata.get("Check/Wire No"));
			selectDDByVisibleText("PaymentReceivedPaymentType_xpath", Transactiondata.get("Premium Received Type"));
			input("PaymentReceivedAmount_xpath", Transactiondata.get("Premium Received Amount"));
			explicitwait("PaymentReceivedSave_xpath", 10, "ElementToBeClickable");
			click("PaymentReceivedSave_xpath");
			explicitwait("PaymentsEdit_xpath", 10, "ElementToBeClickable");
			click("PaymentsEdit_xpath");
			explicitwait("DebitTransactionsSelectToApply_xpath", 20, "ElementToBeClickable");
			click("DebitTransactionsSelectToApply_xpath");
			int numberOfTransaction = getElements("DebitTransactionsRows_xpath").size(); // No of Transactions

			for (int transaction = 1; transaction <= numberOfTransaction; transaction++) {

				String replaceValue = String.valueOf(transaction);
				String updatedLocator = replacementInLocator("DebitTransactionsParticularCheckBox_xpath", replaceValue);
				getElementWithReplacementValue(updatedLocator, "xpath").click();
			}
			click("CreditItemsSave_xpath");
			Thread.sleep(2000);
			click("PaymentsClose_xpath");
			Thread.sleep(2000);
			driver.switchTo().window(mainWindow);
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(2000);
			logger.log(LogStatus.INFO, "AR closed with same user");
			arApprovedByDifferentUser(testName, Transactiondata);			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);
			click("Apply_xpath");
			Thread.sleep(2000);
			click("Save_xpath");
			Thread.sleep(5000);
			System.out.println("AR closed successfully------- ");
			fnLogout();
			loginObj.login("CI_TC_11", "Processor_ID");
			reportingPass(logger, "AR Closed successfully form Approver", testName, nameofCurrMethod);
		} catch (Exception e) {
			e.printStackTrace();
			reportingFail(logger, "Issue in AR Close", testName, nameofCurrMethod);
		}
	}
	
	public void arApprovedByDifferentUser(String testName, HashMap<String, String> Transactiondata) throws Exception {
		
		
		loginWithDifferentUserID(testName);
		searchAnnuitantAltIdAndClickEdit("Annuitant", testName);
		explicitwait("Contracts&Certs_xpath", 20, "ElementToBeClickable");
		click("Contracts&Certs_xpath");
		Thread.sleep(2000);
		switchToFrame("tapestry");
		click("C&CTransactions_xpath");
		Thread.sleep(2000);			
		selectDDByVisibleText("TransStatus_xpath", "All");			
		int numberOfTransactionRow = getElements("TransactionPaymentRows_xpath").size();			
		for(int row = 1; row <= numberOfTransactionRow; row++) {
			
			String replaceValue = String.valueOf(row);
			String updatedTransactionPaymentRowStatusLocator = replacementInLocator("TransactionPaymentRowStatus_xpath", replaceValue);
			String updatedTransactionPaymentRowCogwheelLocator = replacementInLocator("TransactionPaymentRowCogwheel_xpath", replaceValue);
			String updatedTransactionPaymentRowCogwheelEditDetailsLocator = replacementInLocator("TransactionPaymentRowCogwheelEditDetails_xpath", replaceValue);
			
			String status =  getElementWithReplacementValue(updatedTransactionPaymentRowStatusLocator, "xpath").getText();
			if(status.equalsIgnoreCase("Open")) {
				getElementWithReplacementValue(updatedTransactionPaymentRowCogwheelLocator, "xpath").click();
				getElementWithReplacementValue(updatedTransactionPaymentRowCogwheelEditDetailsLocator, "xpath").click();
				Thread.sleep(2000);
				popuphandler();
				Thread.sleep(2000);
				int detailsTotalRow = getElements("EditDetailsOfTransactionDetailsRows_xpath").size();
				for(int detailsRow = 1; detailsRow <= detailsTotalRow; detailsRow++) {
					
					String detailsRowReplaceValue = String.valueOf(detailsRow);
					String updatedDetailsRowStatusLocator = replacementInLocator("EditDetailsOfTransactionDetailsParticularRow_xpath", detailsRowReplaceValue);
					String detailsRowStatus = getElementWithReplacementValue(updatedDetailsRowStatusLocator, "xpath").getText();
					
					if(detailsRowStatus.equalsIgnoreCase("Pending")) {
						
						String updatedDetailsRowCogwheelLocator = replacementInLocator("EditDetailsOfTransactionDetailsCogwheel_xpath", detailsRowReplaceValue);
						getElementWithReplacementValue(updatedDetailsRowCogwheelLocator, "xpath").click();
						explicitwait("DetailsApprove_xpath", 10, "ElementToBeClickable");
						click("DetailsApprove_xpath");
						Thread.sleep(3000);
						
					}
				}
				//explicitwait("TransactionStatusClosed_xpath", 10, "visibilityOfElementLocated");
				clearValue("EditDetailsOfTransactionIssueDate_xpath");
				input("EditDetailsOfTransactionIssueDate_xpath", Transactiondata.get("Issue_Date") + Keys.TAB);	
				explicitwait("EditDetailsOfTransactionApply_xpath", 10, "ElementToBeClickable");
				click("EditDetailsOfTransactionApply_xpath");
				explicitwait("EditDetailsOfTransactionApply_xpath", 10, "ElementToBeClickable");
				click("EditDetailsOfTransactionSave_xpath");
				Thread.sleep(2000);
			}
			row = 1;
			numberOfTransactionRow--;
		}
	}
}
