package testLogic;

public class VariableInParentClass {
	
	protected static boolean functionStatus;
	
	public VariableInParentClass() {
		
		VariableInParentClass.functionStatus = true; 
	}
	
	public static void main(String[] args) {
		
		System.out.println("Varaible In Parent Class");
		System.out.println("Varaible In Parent Class---"+functionStatus);
	}

}
